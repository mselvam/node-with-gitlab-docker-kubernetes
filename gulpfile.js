var gulp = require('gulp');
var sass = require('gulp-sass');
var concat = require('gulp-concat');
var minifyCss = require('gulp-minify-css');
var htmlreplace = require('gulp-html-replace');
var uglify = require('gulp-uglify');
var jshint = require('gulp-jshint');
var browserSync = require('browser-sync').create();
var templateCache = require('gulp-angular-templatecache');

var paths = {
  dev: {
    js: [
      './src/app/app.config.js',
      './src/app/app.module.js',
      './src/app/core/app.routes.js',
      './src/app/core/app.controller.js',
      './src/app/core/app.constants.js',
      './src/app/core/components/uiBreadcrumbs/uiBreadcrumbs.js',
      './src/app/core/components/header/header.directive.js',
      './src/app/core/components/profile/profile.directive.js',
      './src/app/core/components/nav/nav.directive.js',
      './src/app/core/components/page-header/page-header.directive.js',
      './src/app/core/directives/theme-behaviour.directive.js',
      './src/app/core/directives/notify-changes-unsaved.directive.js',
      './src/app/core/directives/delete-item.directive.js',
      './src/app/core/services/upload-images-restangular.js',
      './src/app/core/services/session.services.js',
      './src/app/core/services/query-data-filter.services.js',
      './src/app/core/filters/filter-by-id-list.filter.js',
      './src/app/auth/auth.routes.js',
      './src/app/auth/auth.services.js',
      './src/app/auth/login/login.controller.js',
      './src/app/profile/profile.routes.js',
      './src/app/profile/profile.controller.js',
      './src/app/dashboard/dashboard.routes.js',
      './src/app/dashboard/dashboard.services.js',
      './src/app/dashboard/dashboard.controller.js',
      './src/app/clients/clients.routes.js',
      './src/app/clients/clients.services.js',
      './src/app/clients/clients.controller.js',
      './src/app/clients/client/client.controller.js',
      './src/app/clients/client/client-cards/client-cards.directive.js',
      './src/app/staff/staff.routes.js',
      './src/app/staff/staff.services.js',
      './src/app/staff/staff.controller.js',
      './src/app/staff/view-staff/view-staff.controller.js',
      './src/app/schedule/schedule.routes.js',
      './src/app/schedule/schedule.services.js',
      './src/app/schedule/schedule.controller.js',
      './src/app/schedule/add-appointment/add-appointment.controller.js',
      './src/app/staff-schedule/staff-schedule.routes.js',
      './src/app/staff-schedule/staff-schedule.services.js',
      './src/app/staff-schedule/staff-schedule.controller.js',
      './src/app/staff-schedule/add-staff-schedule/add-staff-schedule.controller.js',
      './src/app/products/products.routes.js',
      './src/app/products/products.services.js',
      './src/app/products/products/products.controller.js',
      './src/app/products/products/product/product.controller.js',
      './src/app/products/orders/orders.controller.js',
      './src/app/products/orders/order/order.controller.js',
      './src/app/products/categories/categories.controller.js',
      './src/app/products/categories/category/category.controller.js',
      './src/app/services/services.routes.js',
      './src/app/services/services.services.js',
      './src/app/services/services/services.controller.js',
      './src/app/services/services/service/service.controller.js',
      './src/app/services/categories/categories.controller.js',
      './src/app/services/categories/category/category.controller.js',
      './src/app/services/orders/orders.controller.js',
      './src/app/services/orders/order/order.controller.js',
      './src/app/messages/messages.routes.js',
      './src/app/messages/messages.services.js',
      './src/app/messages/messages.controller.js'
    ],
    scss: [
      './src/theme-inspinia/**/*.scss',
      './src/app/**/*.scss'
    ],
    css: './src/styles/',
    csslibs: [
        './src/libs/bootstrap/dist/css/bootstrap.min.css',
        './src/libs/fullcalendar/dist/fullcalendar.min.css',
        './src/libs/select2/select2.css',
        './src/libs/angular-toastr/dist/angular-toastr.min.css',
        './src/libs/kjvelarde-angular-multiselectsearchtree/dist/kjvelarde-multiselect-searchtree.min.css'
    ],
    html: './src/**/*.html',
    libs: [
      './src/libs/jquery/dist/jquery.min.js',
      './src/libs/bootstrap/dist/js/bootstrap.min.js',
      './src/libs/moment/min/moment.min.js',
      './src/libs/angular/angular.js',
      './src/libs/angular-ui-router/release/angular-ui-router.js',
      './src/libs/lodash/lodash.js',
      './src/libs/restangular/src/restangular.js',
      './src/libs/ngstorage/ngStorage.min.js',
      './src/libs/angular-bootstrap/ui-bootstrap.min.js',
      './src/libs/angular-bootstrap/ui-bootstrap-tpls.min.js',
      './src/libs/angular-ui-calendar/src/calendar.js',
      './src/libs/fullcalendar/dist/fullcalendar.min.js',
      './src/libs/fullcalendar/dist/gcal.js',
      './src/libs/angular-ui-mask/dist/mask.min.js',
      './src/libs/select2/select2.min.js',
      './src/libs/angular-ui-select2/src/select2.js',
      './src/libs/angular-toastr/dist/angular-toastr.tpls.min.js',
      './src/libs/kjvelarde-angular-multiselectsearchtree/dist/kjvelarde-multiselect-searchtree.min.js',
      './src/libs/kjvelarde-angular-multiselectsearchtree/dist/kjvelarde-multiselect-searchtree.tpl.js'
    ],
    assets: './src/assets/**/*'
  },
  prod: {
    dest: './www/',
    js: './www/js/',
    css: './www/css/',
    assets: './www/assets/'
  }
};

// Concat *.scss files to app.min.css
gulp.task('scss', function() {
  return gulp
    .src(paths.dev.scss)
    .pipe(sass())
    .on('error', sass.logError)
    .pipe(minifyCss({ keepSpecialComments: 0 }))
    .pipe(concat('app.min.css'))
    .pipe(gulp.dest(paths.dev.css))
    .pipe(browserSync.reload({stream: true}));
});

gulp.task('html-to-template-js', function () {
  paths.dev.js = paths.dev.js.concat(['./src/templates.js']);
  return gulp.src(paths.dev.html)
      .pipe(templateCache('templates.js', {module:'speedspa', standalone:false}))
      .pipe(gulp.dest('./src'));
});

// Detect errors and potential problems in JavaScript code
gulp.task('jshint', function() {
  return gulp.src(paths.dev.js)
    .pipe(jshint())
    .pipe(jshint.reporter('default'))
    .pipe(browserSync.reload({stream: true}));
});

// Run liverload server
gulp.task('browser-sync', function() {
  return browserSync.init({
    server: {
      baseDir: './src/'
    }
  });
});

// Local development
gulp.task('watch', ['browser-sync', 'scss'], function() {
  gulp.watch(paths.dev.scss, ['scss']);
  gulp.watch(paths.dev.js, ['jshint']);
  gulp.watch(paths.dev.html).on('change', browserSync.reload);
});

// Replace 'link' and 'script' tags
gulp.task('replace-html', function() {
  return gulp
    .src('./src/index.html')
    .pipe(htmlreplace({
      'css': './css/app.min.css',
      'js': './js/app.min.js'
    }))
    .pipe(gulp.dest(paths.prod.dest));
});

// Copy src/assets to www/assets
gulp.task('copy-config', function() {
  return gulp.src(['./src/env/index.js']).pipe(gulp.dest('./www/env'));
});

// Copy src/assets to www/assets
gulp.task('copy-assets', function() {
  return gulp.src(['./src/assets/**/*']).pipe(gulp.dest('./www/assets/'));
});

// Copy src/fonts to www/fonts
gulp.task('copy-fonts', function() {
  return gulp.src(['./src/fonts/**/*']).pipe(gulp.dest('./www/fonts/'));
});

// Concat and minify libs with our *.js files in one
gulp.task('minify-js', ['html-to-template-js'], function() {
  var concatedPathes = paths.dev.libs.concat(paths.dev.js);
  return gulp
    .src(concatedPathes)
    .pipe(uglify({ mangle: false }))
    .pipe(concat('app.min.js'))
    .pipe(gulp.dest(paths.prod.js));
});

// Transfet app.min.css file to prod folder
gulp.task('transfer-css', function() {
  var cssPaths = paths.dev.csslibs.concat([paths.dev.css + 'app.min.css']);
  return gulp
    .src(cssPaths)
    .pipe(minifyCss({ keepSpecialComments: 0 }))
    .pipe(concat('app.min.css'))
    .pipe(gulp.dest(paths.prod.css));
});

// Build prod folder
gulp.task('build-prod', [
  'copy-config',
  'minify-js',
  'transfer-css',
  'replace-html',
  'copy-assets',
  'copy-fonts'
]);

// Run liverload server
gulp.task('browser-sync-www', function() {
  return browserSync.init({
    server: {
      baseDir: './www/'
    }
  });
});

gulp.task('default', ['watch']);
