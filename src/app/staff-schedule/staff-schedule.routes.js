(function() {
    'use strict';
    angular
        .module('speedspa.staffSchedule', ['ui.router','ui.calendar', 'ui.bootstrap'])
        .config(routerConfig);

    routerConfig.$inject = [
        '$stateProvider'
    ];

    function routerConfig(
        $stateProvider
    ) {

        $stateProvider
            .state('index.staffschedule', {
                abstract: true,
                template: "<div data-ui-view></div>",
                data: {
                    proxy: 'index.staffschedule.staffschedule',
                    permissions: ['stylist','admin']
                }
            })
            .state('index.staffschedule.staffschedule', {
                url: "/staff-schedule",
                templateUrl: "app/staff-schedule/staff-schedule.html",
                controller: 'StaffScheduleCtrl',
                controllerAs: 'vm',
                data: {
                    bcName: 'Staff Schedule'
                },
                resolve: {
                    staffScheduleResolve: staffScheduleResolve,
                    staffResolve: staffResolve
                }
            })
            .state('index.staffschedule.addstaffschedule', {
                url: "/staff-schedule/add-staff-schedule",
                templateUrl: "app/staff-schedule/add-staff-schedule/add-staff-schedule.html",
                controller: 'AddStaffScheduleCtrl',
                controllerAs: 'vm',
                data: {
                    bcName: 'Add New Staff Schedule Item',
                    permissions: ['stylist','admin']
                },
                resolve: {
                    staffResolve: staffResolve
                }
            })
    }

    function staffScheduleResolve(StaffScheduleService, $rootScope, $localStorage, QueryDataFilterService, $q){
        var start = new Date(moment().startOf('month').subtract(1, 'milliseconds'));
        start.setTime( start.getTime() - start.getTimezoneOffset()*60*1000 );
        start = start.toISOString();
        var end = new Date(moment().endOf('month').add(1, 'milliseconds'));
        end.setTime( end.getTime() - end.getTimezoneOffset()*60*1000 );
        end = end.toISOString();
        var data = {
            timeStart: {
                $lte: end,
                $gt: start
            }
        };
        if($rootScope.userRole == 'stylist') {
            data.resourceId = {
                '$in': [$localStorage.user.id]
            };
        }
        var query = QueryDataFilterService.buildFilter(data);
        return StaffScheduleService.getStaffSchedule(query).then(function(res) {
            return res;
        }).catch(function(err) {
            console.log(err, 'error');
            return [];
        });
    }

    function staffResolve(StaffService, $rootScope, $localStorage, $q){
        if($rootScope.userRole == 'admin')
            return StaffService.getStaffList('', 'stylist').then(function(res) {
                return res;
            }).catch(function(err) {
                console.log(err, 'error');
                return [];
            });
        else return [$localStorage.user];
    }

})();
