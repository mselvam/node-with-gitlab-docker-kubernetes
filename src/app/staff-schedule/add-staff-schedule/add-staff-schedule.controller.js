(function() {
    'use strict';

    angular.module('speedspa.staffSchedule')
        .controller('AddStaffScheduleCtrl', AddStaffScheduleCtrl);

    AddStaffScheduleCtrl.$inject = [
        '$scope',
        'StaffScheduleService',
        'staffResolve',
        '$state',
        '$timeout'
    ];

    function AddStaffScheduleCtrl(
        $scope,
        StaffScheduleService,
        staffResolve,
        $state,
        $timeout
    ) {

        var vm = this;

        vm.staff = staffResolve;
        vm.event = {
            resourceId: null,
            period: 'weekly',
            startDate: new Date(),
            endDate: new Date(),
            startTime: new Date(moment().startOf('day')),
            endTime: new Date(moment().startOf('day'))
        };
        vm.popupDateTimeStart = {
            opened: false,
            dateOptions: {
                formatYear: 'yy',
                maxDate: vm.event.startDate,
                startingDay: 1
            }
        };
        vm.popupDateTimeEnd = {
            opened: false,
            dateOptions: {
                formatYear: 'yy',
                minDate: vm.event.endDate,
                startingDay: 1
            }
        };
        vm.occurences = [
            {
                value: 'monthly',
                name: 'Monthly'
            },
            {
                value: 'weekly',
                name: 'Weekly'
            },
            {
                value: 'daily',
                name: 'Daily'
            }
        ];
        vm.weekly = [
            {
                value: 'false',
                day: 0,
                name: 'S'
            },
            {
                value: 'false',
                day: 1,
                name: 'M'
            },
            {
                value: 'false',
                day: 2,
                name: 'T'
            },
            {
                value: 'false',
                day: 3,
                name: 'W'
            },
            {
                value: 'false',
                day: 4,
                name: 'T'
            },
            {
                value: 'false',
                day: 5,
                name: 'F'
            },
            {
                value: 'false',
                day: 6,
                name: 'S'
            }
        ];
        vm.monthly = _.range(1, 32).map(function square(n) {
            return {
                value: 'false',
                day: n
            };
        });
        vm.daily = 1;
        vm.dataWasSaved = false;
        vm.saving = false;
        vm.errors = '';

        vm.openDate = openDate;
        vm.cancel = cancel;
        vm.save = save;

        $scope.$watch('vm.event.startDate', function(v){
            vm.popupDateTimeEnd.dateOptions.minDate = v;
        });

        $scope.$watch('vm.event.endDate', function(v){
            vm.popupDateTimeStart.dateOptions.maxDate = v;
        });

        function openDate(date){
            date.opened = true;
        }

        function cancel(){
            vm.addnew = true;
            $state.go('index.staffschedule.staffschedule');
            vm.saving = false;
        }

        function save(){
            vm.dataWasSaved = true;
            vm.saving = true;

            var days = [];
            if(vm.event.period == 'weekly'){
                angular.forEach(vm.weekly, function(value,key){
                    if(value.value == true) days.push(value.day);
                });
            }
            else if(vm.event.period == 'monthly'){
                angular.forEach(vm.monthly, function(value,key){
                    if(value.value == true) days.push(value.day);
                });
            }
            else if(vm.event.period == 'daily'){
                days = [vm.daily];
            }

            var item = {
                repeatingStartTime: moment(new Date(vm.event.startTime)).format('h:mm a'),
                repeatingEndTime: moment(new Date(vm.event.endTime)).format('h:mm a'),
                repeatingStartDate: moment(vm.event.startDate).format('YYYY-MM-DD'),
                repeatingEndDate: moment(vm.event.endDate).format('YYYY-MM-DD'),
                period: vm.event.period,
                frequency: days,
                resourceId: vm.event.resourceId
            };
            if(checkData() && days.length>0) {
                StaffScheduleService.addStaffScheduleGenerate(item).then(function(res) {
                    vm.saving = false;
                    vm.errors = '';
                    $state.go('index.staffschedule.staffschedule');
                }).catch(function(err) {
                    console.log(err, 'error');
                    vm.errors = 'Can not save your data. '+err.data.message;
                    vm.saving = false;
                });
            }
            else {
                vm.errors = 'Please check data you input.';
                vm.dataWasSaved = false;
                vm.saving = false;
            }
        }

        function checkData(){
            if(
                (typeof (vm.event.startTime).getMonth === 'function')
                && (typeof (vm.event.endTime).getMonth === 'function' )
                && (vm.event.endTime > vm.event.startTime )
                && (typeof (vm.event.startDate).getMonth === 'function' )
                && (typeof (vm.event.endDate).getMonth === 'function' )
                && ( vm.event.period && vm.event.period.length >0 )
                && ( vm.event.resourceId && vm.event.resourceId.length >0 )
            ) return true;
            else return false;
        }

    }
})();
