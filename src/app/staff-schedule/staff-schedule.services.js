(function() {
    'use strict';
    angular
        .module('speedspa.staffSchedule')
        .factory('StaffScheduleService', StaffScheduleService);

    StaffScheduleService.$inject = [
        'Restangular',
        '$q'
    ];

    function StaffScheduleService(
        Restangular,
        $q
    ) {
        var service = {
            getStaffSchedule: getStaffSchedule,
            getStaffScheduleItem: getStaffScheduleItem,
            addStaffScheduleItem: addStaffScheduleItem,
            updateStaffScheduleItem: updateStaffScheduleItem,
            deleteStaffScheduleItem: deleteStaffScheduleItem,
            addStaffScheduleGenerate: addStaffScheduleGenerate
        };

        return service;

        function getStaffSchedule(query){
            return Restangular
                .all('resources/schedules')
                .getList(query)
                .then(getScheduleComplete)
                .catch(getScheduleFailed);

            function getScheduleComplete(res) {
                return res.plain();
            }

            function getScheduleFailed(err) {
                return $q.reject(err);
            }
        }

        function getStaffScheduleItem(id){
            return Restangular
                .all('resources/schedules')
                .get(id)
                .then(getScheduleItemComplete)
                .catch(getScheduleItemFailed);

            function getScheduleItemComplete(res) {
                return res.plain();
            }

            function getScheduleItemFailed(err) {
                return $q.reject(err);
            }
        }

        function addStaffScheduleItem(item){
            return Restangular
                .all('resources/schedules')
                .post(item)
                .then(addScheduleItemComplete)
                .catch(addScheduleItemFailed);

            function addScheduleItemComplete(res) {
                return res.plain();
            }

            function addScheduleItemFailed(err) {
                return $q.reject(err);
            }
        }

        function updateStaffScheduleItem(item){
            return Restangular
                .one('resources/schedules', item.id)
                .patch(item)
                .then(updateScheduleItemComplete)
                .catch(updateScheduleItemFailed);

            function updateScheduleItemComplete(res) {
                return res.plain();
            }

            function updateScheduleItemFailed(err) {
                return $q.reject(err);
            }
        }

        function deleteStaffScheduleItem(item){
            return Restangular
                .one('resources/schedules', item.id)
                .remove()
                .then(deleteScheduleItemComplete)
                .catch(deleteScheduleItemFailed);

            function deleteScheduleItemComplete(res) {
                return res.plain();
            }

            function deleteScheduleItemFailed(err) {
                return $q.reject(err);
            }
        }

        function addStaffScheduleGenerate(item){
            return Restangular
                .all('resources/schedules/generate')
                .post(item)
                .then(addAppointmentComplete)
                .catch(addAppointmentFailed);

            function addAppointmentComplete(res) {
                return res.plain();
            }

            function addAppointmentFailed(err) {
                return $q.reject(err);
            }
        }
    }

})();
