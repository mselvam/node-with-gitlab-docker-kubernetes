(function() {
    'use strict';

    angular.module('speedspa.staffSchedule')
        .controller('StaffScheduleCtrl', StaffScheduleCtrl);

    StaffScheduleCtrl.$inject = [
        'uiCalendarConfig',
        '$timeout',
        'StaffScheduleService',
        'QueryDataFilterService',
        'staffScheduleResolve',
        'staffResolve',
        '$state',
        '$anchorScroll',
        '$rootScope',
        '$localStorage',
        '$scope'
    ];

    function StaffScheduleCtrl(
        uiCalendarConfig,
        $timeout,
        StaffScheduleService,
        QueryDataFilterService,
        staffScheduleResolve,
        staffResolve,
        $state,
        $anchorScroll,
        $rootScope,
        $localStorage,
        $scope
    ) {

        var vm = this;

        var dates = {
            loaded: {
                start: new Date(moment().startOf('month').subtract(1, 'milliseconds')),
                end: new Date(moment().endOf('month').add(1, 'milliseconds'))
            },
            toload: {
                start: new Date(moment().startOf('month').subtract(1, 'milliseconds')),
                end: new Date(moment().endOf('month').add(1, 'milliseconds'))
            }
        };
        vm.schedule = staffScheduleResolve;
        vm.staff = staffResolve;
        vm.buttons = [{
            title: 'Add New Staff Schedule Item',
            callback: goToAddStaffScheduleItemPage
        }];
        vm.searchQuery = {
            staff: ''
        };
        vm.popupDateTimeStart = {
            opened: false,
            dateOptions: {
                formatYear: 'yy',
                maxDate: new Date(2020, 5, 22),
                minDate: new Date(),
                startingDay: 1
            }
        };
        vm.loading = true;
        vm.popupDateTimeEnd = {
            opened: false,
            dateOptions: {
                formatYear: 'yy',
                maxDate: new Date(2020, 5, 22),
                minDate: new Date(),
                startingDay: 1
            }
        };
        vm.eventSources = [];
        vm.uiConfig = {
            calendar: {
                height: 450,
                editable: false,
                header:{
                    left: 'today prev,next',
                    center: 'title',
                    right: 'month agendaWeek agendaDay'
                },
                timeFormat : "H:mma",
                eventClick: editEvent,
                viewRender: eventsLoad
            }
        };
        vm.colors = [
            '#43b394',
            '#3d84c6',
            '#50c6c8',
            '#f4ac59',
            '#ec5564',
            '#c2c2c2',
            '#f1daf2',
            '#0056a8',
            '#884411',
            '#f279b7',
            '#f2797a',
            '#99cdff',
            '#118888',
            '#ccddaa',
            '#8899aa',
            '#3d454c',
            '#aadddd',
            '#bbaadd',
            '#557777',
            '#d8adae',
            '#555577',
            '#992222',
            '#2f4050',
            '#cce6ff',
            '#7f3f40',
            '#778811'
        ];
        vm.editing = false;
        vm.event = {};
        vm.errors = '';
        vm.saving = false;

        vm.search = search;
        vm.reset = reset;
        vm.save = save;
        vm.delete = deleteEvent;
        vm.cancel = cancel;
        vm.shortDateTime = setDateTimeToDeletePopup;
        vm.openDate = openDate;
        vm.reRenderEvents = reRenderEvents;

        $timeout(function(){
            uiCalendarConfig.calendars['ScheduleCalendar'].fullCalendar('render');
            renderEvents();
        }, 300);

        function search(){
            var data = {};
            if(vm.searchQuery.staff && vm.searchQuery.staff.length){
                data.resourceId = {
                    '$eq': vm.searchQuery.staff
                };
            }
            var query = QueryDataFilterService.buildFilter(data);

            StaffScheduleService.getStaffSchedule(query).then(function(res) {
                vm.schedule = res;

                reRenderEvents();
            }).catch(function(err) {
                console.log(err, 'error');
                return [];
            })
        }

        function reset(){
            vm.searchQuery = {
                staff: ''
            };
            search();
        }

        function save(){
            vm.saving = true;
            var timeslot = eventToTimeslot(vm.event);

            StaffScheduleService.updateStaffScheduleItem(timeslot).then(function(res) {
                angular.forEach(vm.schedule, function(value, key){
                    if(value.id == res.id){
                        value = res;
                    }
                });

                var eventSourceIndex; var staffSourceIndex;
                _.map(vm.eventSources, function(eventSource, index){
                    if( _.find(eventSource.events, {id:res.id}) ) {
                        eventSourceIndex = index;
                    }
                    if( eventSource.resourceId == res.resourceId ) {
                        staffSourceIndex = index;
                    }
                });

                var resource = _.find(vm.staff, {id:res.resourceId});

                vm.event.start = setDateTimeToShow(res.timeStart);
                vm.event.end = setDateTimeToShow(res.timeEnd);
                vm.event.resourceId = res.resourceId;
                vm.event.resource = resource? resource.name:'Deleted Staff';
                vm.event.startDate = setDateTimeToShow(res.timeStart);
                vm.event.startTime = setDateTimeToShow(res.timeStart);
                vm.event.endTime = setDateTimeToShow(res.timeEnd);
                vm.event.title = ' - '+moment(vm.event.end).format("H:mma");

                var event = {};
                if(eventSourceIndex!==undefined){
                    event = _.find(vm.eventSources[eventSourceIndex].events, {id:res.id});
                    var resource = _.find(vm.staff, {id:res.resourceId});

                    event.start = setDateTimeToShow(res.timeStart);
                    event.end = setDateTimeToShow(res.timeEnd);
                    event.resourceId = res.resourceId;
                    event.resource = resource? resource.name:'Deleted Staff';
                    event.startDate = setDateTimeToShow(res.timeStart);
                    event.startTime = setDateTimeToShow(res.timeStart);
                    event.endTime = setDateTimeToShow(res.timeEnd);
                    event.title = ' - '+moment(event.end).format("H:mma");

                    if( (staffSourceIndex!==undefined) && (staffSourceIndex !== eventSourceIndex) ){
                        var eventToCopy = angular.copy(event);
                        eventToCopy.stick = true;
                        _.remove(vm.eventSources[eventSourceIndex].events, {id:res.id});
                        vm.eventSources[staffSourceIndex].events.push(eventToCopy);
                        vm.event.source = vm.eventSources[staffSourceIndex];
                    }
                }

                uiCalendarConfig.calendars['ScheduleCalendar'].fullCalendar('updateEvent',vm.event);

                vm.saving = false;
                cancel();
            }).catch(function(err) {
                console.log(err, 'error');
                vm.errors = 'Can not save your data. '+err.data.message;
                vm.saving = false;
            });
        }

        function deleteEvent(event){
            return StaffScheduleService.deleteStaffScheduleItem(event).then(function(res) {
                _.map(vm.eventSources, function(eventSource){
                    if( _.find(eventSource.events, {id:res.id}) ) {
                        _.remove(eventSource.events, {
                            id: res.id
                        });
                    }
                });
                uiCalendarConfig.calendars['ScheduleCalendar'].fullCalendar('removeEvents',vm.event.id);
                cancel();
                return res;
            }).catch(function(err) {
                return err;
            });
        }

        function cancel(){
            vm.editing = false;
            vm.event = {};
            vm.errors = '';
            vm.saving = false;
        }

        function openDate(date){
            date.opened = true;
        }

        function renderEvents(){
            angular.forEach(vm.staff, function(value, key){
                var events = _.filter(vm.schedule, {'resourceId': value.id})
                    .map(function(event){
                        return timeslotToEvent(event);
                    });

                var eventSource = {
                    color: vm.colors[key],
                    textColor: '#ffffff',
                    resourceId: value.id,
                    events: events.length? events : []
                };

                vm.eventSources.push(eventSource);
                uiCalendarConfig.calendars['ScheduleCalendar'].fullCalendar('addEventSource',vm.eventSources[vm.eventSources.length-1]);
            });
            vm.loading = false;
        }

        function reRenderEvents(staff,color){
            uiCalendarConfig.calendars['ScheduleCalendar'].fullCalendar('removeEvents');

            angular.forEach(vm.eventSources, function(value, key){
                if(_.find(staff, {id:value.resourceId}))
                    uiCalendarConfig.calendars['ScheduleCalendar'].fullCalendar('addEventSource',value);

            });
        }

        function editEvent(data, event, view){
            vm.event = data;
            vm.editing = true;

            $timeout(function(){$anchorScroll('edit-event');}, 300);
        }

        function eventsLoad(view, element){

            var intervalEnd = new Date(view.intervalEnd);
            intervalEnd.setTime( intervalEnd.getTime() + intervalEnd.getTimezoneOffset()*60*1000 );
            intervalEnd = new Date(moment(intervalEnd).startOf('day').subtract(1,'milliseconds'));

            var intervalStart = new Date(view.intervalStart);
            intervalStart.setTime( intervalStart.getTime() + intervalStart.getTimezoneOffset()*60*1000 );

            if(intervalEnd>dates.loaded.end){
                dates.toload.start = new Date(moment(intervalEnd).startOf('month').subtract(1, 'milliseconds'));
                dates.toload.end = new Date(moment(intervalEnd).endOf('month').add(1, 'milliseconds'));
                dates.loaded.end = new Date(moment(intervalEnd).endOf('month').add(1, 'milliseconds'));
                loadNewPeriod(dates.toload.start,dates.toload.end);
            }
            if(intervalStart<dates.loaded.start){
                dates.toload.start = new Date(moment(dates.loaded.start).startOf('month').subtract(1, 'milliseconds'));
                dates.toload.end = new Date(moment(dates.loaded.start).endOf('month').add(1, 'milliseconds'));
                dates.loaded.start = new Date(moment(dates.loaded.start).startOf('month').subtract(1, 'milliseconds'));
                loadNewPeriod(dates.toload.start,dates.toload.end);
            }
        }

        function loadNewPeriod(start, end){
            var start = start;
            var end = end;

            start.setTime( start.getTime() - start.getTimezoneOffset()*60*1000 );
            start = start.toISOString();

            end.setTime( end.getTime() - end.getTimezoneOffset()*60*1000 );
            end = end.toISOString();

            var data = {
                timeStart: {
                    $lte: end,
                    $gt: start
                }
            };
            if($rootScope.userRole == 'stylist') {
                data.resourceId = {
                    '$in': [$localStorage.user.id]
                };
            }
            var query = QueryDataFilterService.buildFilter(data);
            StaffScheduleService.getStaffSchedule(query).then(function(res) {
                vm.schedule = _.concat(vm.schedule, res);
                angular.forEach(res, function(value,key){
                    var event = timeslotToEvent(value);
                    var source = _.find(vm.eventSources, {resourceId:value.resourceId});
                    if(source) {
                        source.events.push(event);
                        event.source = source;

                        uiCalendarConfig.calendars['ScheduleCalendar'].fullCalendar('renderEvent', event, true);
                    }
                });

                reRenderEvents(vm.staff);
            }).catch(function(err) {
                console.log(err, 'error');
            });
        }

        function setDateTimeToDeletePopup(datetime){
            var datetime = new Date(datetime);
            var dt = moment(datetime).format('dddd, MMMM Do YYYY, h:mm:ss a');
            return dt;
        }
        function setDateTimeToShow(datetime){
            var datetime = new Date(datetime);
            var dt = new Date(datetime.getFullYear(), datetime.getMonth(), datetime.getDate(),  datetime.getHours(), datetime.getMinutes(), datetime.getSeconds());
            dt.setTime( dt.getTime() + dt.getTimezoneOffset()*60*1000 );
            return dt;
        }
        function setDateTimeToSave(date, time){
            var dt = new Date(date.getFullYear(), date.getMonth(), date.getDate(),  time.getHours(), time.getMinutes(), time.getSeconds());
            dt.setTime( dt.getTime() - time.getTimezoneOffset()*60*1000 );
            dt = dt.toISOString();
            return dt;
        }

        function timeslotToEvent(timeslot){
            var resource = _.find(vm.staff, {id:timeslot.resourceId});
            var event = {
                start: setDateTimeToShow(timeslot.timeStart),
                end: setDateTimeToShow(timeslot.timeEnd),
                resourceId: timeslot.resourceId,
                resource: resource? resource.name:'Deleted Staff',
                startDate: setDateTimeToShow(timeslot.timeStart),
                startTime: setDateTimeToShow(timeslot.timeStart),
                endTime: setDateTimeToShow(timeslot.timeEnd),
                allDay :false
            };
            event.title = ' - '+moment(event.end).format("H:mma");
            if(timeslot.id){
                event.id = timeslot.id;
            }
            return event;
        }

        function eventToTimeslot(event){
            var start = setDateTimeToSave(event.startDate, event.startTime);
            var end = setDateTimeToSave(event.startDate, event.endTime);
            var timeslot = {
                timeStart: start,
                timeEnd: end,
                resourceId: event.resourceId
            };
            if(event.id){
                timeslot.id = event.id;
            }
            return timeslot;
        }

        function goToAddStaffScheduleItemPage(){
            $state.go('index.staffschedule.addstaffschedule');
        }

    }
})();
