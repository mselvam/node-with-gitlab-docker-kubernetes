(function() {
    'use strict';
    angular
        .module('speedspa.login')
        .factory('LoginService', LoginService);

    LoginService.$inject = [
        'Restangular',
        '$q'
    ];

    function LoginService(
        Restangular,
        $q
    ) {
        var service = {
            authEmailLogin: authEmailLogin
        };

        return service;

        function authEmailLogin(email, password){
            return Restangular
                .all('auth/email')
                .post({
                    'email':email,
                    'password': password
                })
                .then(authEmailLoginComplete)
                .catch(authEmailLoginFailed);

            function authEmailLoginComplete(res) {
                return res.plain();
            }

            function authEmailLoginFailed(err) {
                return $q.reject(err);
            }
        }
    }

})();
