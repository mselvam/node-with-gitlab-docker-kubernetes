(function() {
    'use strict';
    angular
        .module('speedspa.login', [
            'ui.router',
            'ui.mask'
        ])
        .config(routerConfig);

    routerConfig.$inject = [
        '$stateProvider',
        '$urlRouterProvider'
    ];

    function routerConfig(
        $stateProvider,
        $urlRouterProvider
    ) {

        $stateProvider
            .state('auth', {
                abstract: true,
                templateUrl: "app/auth/auth.html",
                permissions: ['admin','stylist']
            })
            .state('auth.login', {
                url: '/',
                templateUrl: 'app/auth/login/login.html',
                controller: 'EmailLoginCtrl',
                controllerAs: 'vm'
            });
    }

})();
