(function() {
    'use strict';

    angular.module('speedspa.login')
        .controller('EmailLoginCtrl', EmailLoginCtrl);

    EmailLoginCtrl.$inject = [
        'LoginService',
        'SessionService',
        '$state',
        'toastr'
    ];

    function EmailLoginCtrl(
        LoginService,
        SessionService,
        $state,
        toastr
    ) {

        var vm = this;

        vm.emailLogin = emailLogin;

        vm.emailLoginInfo = {
            email: '',
            password: ''
        };
        vm.errors = '';

        function emailLogin(){
            LoginService.authEmailLogin(vm.emailLoginInfo.email, vm.emailLoginInfo.password).then(function(res) {
                if( (_.includes(res.roles, 'admin')) || (_.includes(res.roles, 'stylist')) ){
                    var user = res;
                    var token = res.token;
                    delete user.password;
                    delete user.token;
                    SessionService.sessionCreate(user, token);

                    $state.go('index.schedule.schedule');
                }
                else {
                    vm.errors = 'You are not admin, you can not enter this app.';
                }
            }).catch(function(res) {
                var message;
                if (res.data) {
                    message = res.data.message;
                } else {
                    message = 'Oops something went wrong!!';
                }
                vm.errors = message;
                toastr.error(message);
            });
        }

    }
})();
