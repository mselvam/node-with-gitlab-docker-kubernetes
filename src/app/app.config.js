(function() {

  var env = {};

  // Import variables if present (from env.js)
  if(window){
    Object.assign(env, window.__env);
  }

  angular
    .module('speedspa', [
      'restangular',
      'speedspa.core',
      'speedspa.login',
      'speedspa.profile',
      'speedspa.dashboard',
      'speedspa.clients',
      'speedspa.staff',
      'speedspa.staffSchedule',
      'speedspa.schedule',
      'speedspa.products',
      'speedspa.services',
      'speedspa.messages'
    ])
    .constant('siteConfigs', env)
    .config(configure);

  configure.$inject = [
    'RestangularProvider',
    '$locationProvider',
    'siteConfigs',
    'toastrConfig'
  ];

  function configure(
    RestangularProvider,
    $locationProvider,
    siteConfigs,
    toastrConfig
  ) {

    $locationProvider.hashPrefix('');
    RestangularProvider.setBaseUrl(siteConfigs.api? siteConfigs.api : 'http://server.staging.speedspa.tep.pw');
    RestangularProvider.setDefaultHeaders({ 'Content-Type': 'application/json' });

    angular.extend(toastrConfig, {
      autoDismiss: true,
      containerId: 'toast-container',
      timeOut: 5000,
      maxOpened: 8000,
      newestOnTop: true,
      positionClass: 'toast-bottom-right',
      preventDuplicates: false,
      preventOpenDuplicates: false,
      target: 'body'
    });
  }

})();
