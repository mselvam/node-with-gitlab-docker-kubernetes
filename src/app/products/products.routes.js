(function() {
    'use strict';
    angular
        .module('speedspa.products', [
            'ui.router',
            'ui.bootstrap',
            'multiselect-searchtree'
        ])
        .config(routerConfig);

    routerConfig.$inject = [
        '$stateProvider'
    ];

    function routerConfig(
        $stateProvider
    ) {

        $stateProvider
            .state('index.products', {
                url: "/products",
                abstract: true,
                template: "<div data-ui-view></div>",
                data: {
                    proxy: 'index.products.products',
                    permissions: ['admin']
                }
            })
            .state('index.products.products', {
                url: "/",
                templateUrl: "app/products/products/products.html",
                controller: 'ProductsCtrl',
                controllerAs: 'vm',
                data: {
                    bcName: 'Products'
                },
                resolve: {
                    productsResolve: productsResolve
                }
            })
            .state('index.products.addnew', {
                url: "/add-new",
                templateUrl: "app/products/products/product/product.html",
                controller: 'ProductCtrl',
                controllerAs: 'vm',
                data: {
                    bcName: 'New Product'
                },
                resolve: {
                    productsGetProductResolve: productsGetEmptyProductResolve,
                    productsCategoriesResolve: productsCategoriesResolve
                }
            })
            .state('index.products.view', {
                url: "/view/:productId",
                templateUrl: "app/products/products/product/product.html",
                controller: 'ProductCtrl',
                controllerAs: 'vm',
                data: {
                    bcName: 'Product View/Edit'
                },
                resolve: {
                    productsGetProductResolve: productsGetProductResolve,
                    productsCategoriesResolve: productsCategoriesResolve
                }
            })
            .state('index.products.orders', {
                url: "/orders",
                abstract: true,
                template: "<div data-ui-view></div>",
                data: {
                    proxy: 'index.products.orders.all'
                }
            })
            .state('index.products.orders.all', {
                url: "/",
                templateUrl: "app/products/orders/orders.html",
                controller: 'OrdersCtrl',
                controllerAs: 'vm',
                data: {
                    bcName: 'Orders'
                },
                resolve: {
                    productsOrdersResolve: productsOrdersResolve,
                    clientsResolve: clientsResolve
                }
            })
            .state('index.products.orders.addnew', {
                url: "/add-new",
                templateUrl: "app/products/orders/order/order.html",
                controller: 'OrderCtrl',
                controllerAs: 'vm',
                data: {
                    bcName: 'New Order'
                },
                resolve: {
                    productsGetOrderResolve: productsGetEmptyOrderResolve,
                    productsResolve: productsResolve,
                    clientsResolve: clientsResolve
                }
            })
            .state('index.products.orders.view', {
                url: "/view/:orderId",
                templateUrl: "app/products/orders/order/order.html",
                controller: 'OrderCtrl',
                controllerAs: 'vm',
                data: {
                    bcName: 'Order View/Edit'
                },
                resolve: {
                    productsGetOrderResolve: productsGetOrderResolve,
                    productsResolve: productsResolve,
                    clientsResolve: clientsResolve
                }
            })
            .state('index.products.categories', {
                url: "/categories",
                abstract: true,
                template: "<div data-ui-view></div>",
                data: {
                    proxy: 'index.products.categories.all'
                }
            })
            .state('index.products.categories.all', {
                url: "/",
                templateUrl: "app/products/categories/categories.html",
                controller: 'CategoriesCtrl',
                controllerAs: 'vm',
                data: {
                    bcName: 'Categories'
                },
                resolve: {
                    productsCategoriesResolve: productsCategoriesResolve
                }
            })
            .state('index.products.categories.addnew', {
                url: "/add-new",
                templateUrl: "app/products/categories/category/category.html",
                controller: 'CategoryCtrl',
                controllerAs: 'vm',
                data: {
                    bcName: 'New Category'
                },
                resolve: {
                    productsGetCategoryResolve: productsGetEmptyCategoryResolve,
                    productsCategoriesResolve: productsCategoriesResolve
                }
            })
            .state('index.products.categories.view', {
                url: "/view/:categoryId",
                templateUrl: "app/products/categories/category/category.html",
                controller: 'CategoryCtrl',
                controllerAs: 'vm',
                data: {
                    bcName: 'Category View/Edit'
                },
                resolve: {
                    productsGetCategoryResolve: productsGetCategoryResolve,
                    productsCategoriesResolve: productsCategoriesResolve
                }
            })
    }

    function productsResolve(ProductsService, $q){
        return ProductsService.getProducts().then(function(res) {
            return res.data? res.data:[];
        }).catch(function(err) {
            console.log(err, 'error');
            return [];
        });
    }

    function productsGetEmptyProductResolve(){
        return {
            name: '',
            caption: '',
            description: '',
            categoriesIds: [],
            active: true,
            images: [],
            price: 20,
            quantity: 1,
            metaTagTitle: '',
            metaTagDescription: '',
            metaTagKeywords: '',
            model: '',
            location: '',
            taxClass: '',
            minQuantity: 0,
            sortOrder: 0
        }
    }

    function productsGetProductResolve(ProductsService, $q, $stateParams){
        return ProductsService.getProduct($stateParams.productId).then(function(res) {
            return res;
        }).catch(function(err) {
            console.log(err, 'error');
            return {};
        });
    }

    function productsOrdersResolve(ProductsService, $q){
        return ProductsService.getOrdersReport().then(function(res) {
            var orders = (res.data && res.data.data)? res.data.data:[];
            var total = (res.data && res.data.total)? res.data.total:0;
            var limit = (res.data && res.data.limit)? res.data.limit:0;
            if(total>limit && limit>0) {
                var requestsNumber = Math.ceil(total/limit);
                var ordersPromise = Promise.resolve("cat");
                var ordersToGetListF = [];
                var numbers = _.range(1, requestsNumber);
                angular.forEach(numbers, function(value, key){
                    var get100 = ordersPromise.then(function () {
                        var query = {
                            '$skip':value*100
                        };
                        return ServicesService.getOrdersReport(query).then(function(skipRes) {
                            var skipOrders = (skipRes.data && skipRes.data.data)? skipRes.data.data:[];
                            orders = orders.concat(skipOrders);
                            return true;
                        }).catch(function(err) {
                            console.log(err, 'error');
                            return [];
                        });
                    });
                    ordersToGetListF.push(get100);
                });
                return $q.all(ordersToGetListF).then(function(){
                    angular.forEach(orders, function(value, key){
                        value.created = new Date(value.created*1000);
                        value.updated = new Date(value.updated*1000);
                        if(!(value.tip && value.tip.amount && value.tip.amount>0)){
                            value.tip = {
                                amount: 0
                            }
                        }
                        value.total = value.amount + value.tip.amount
                    });
                    return orders;
                },function(err){
                    console.log(err, 'error');
                });
            }
            else {
                angular.forEach(orders, function(value, key){
                    value.created = new Date(value.created*1000);
                    value.updated = new Date(value.updated*1000);
                    if(!(value.tip && value.tip.amount && value.tip.amount>0)){
                        value.tip = {
                            amount: 0
                        }
                    }
                    value.total = value.amount + value.tip.amount
                });
                return orders;
            }
        }).catch(function(err) {
            console.log(err, 'error');
            return [];
        });
    }

    function productsGetEmptyOrderResolve(){
        return {
            items: [],
            clientId: '',
            type: 'products'
        }
    }

    function productsGetOrderResolve(ProductsService, $q, $stateParams){
        return ProductsService.getOrder($stateParams.orderId).then(function(res) {
            return res;
        }).catch(function(err) {
            console.log(err, 'error');
            return {};
        });
    }

    function clientsResolve(ClientsService, $q){
        return ClientsService.getClients().then(function(res) {
            return res;
        }).catch(function(err) {
            console.log(err, 'error');
            return [];
        });
    }

    function productsCategoriesResolve(ProductsService, $q){
        return ProductsService.getCategories().then(function(res) {
            return res;
        }).catch(function(err) {
            console.log(err, 'error');
            return [];
        });
    }

    function productsGetEmptyCategoryResolve(){
        return {
            name: '',
            parentId: null
        }
    }

    function productsGetCategoryResolve(ProductsService, $q, $stateParams){
        return ProductsService.getCategory($stateParams.categoryId).then(function(res) {
            return res;
        }).catch(function(err) {
            console.log(err, 'error');
            return {};
        });
    }

})();
