(function() {
    'use strict';

    angular.module('speedspa.products')
        .controller('CategoriesCtrl', CategoriesCtrl);

    CategoriesCtrl.$inject = [
        'productsCategoriesResolve',
        'ProductsService',
        '$state',
        '$filter'
    ];

    function CategoriesCtrl(
        productsCategoriesResolve,
        ProductsService,
        $state,
        $filter
    ) {

        var vm = this;

        vm.categories = productsCategoriesResolve;
        vm.plainCategories = [];
        vm.total = vm.plainCategories.length;
        vm.currentPage = 1;
        vm.itemsPerPage = 10;
        vm.orderby = {
            field: '',
            desc: false
        };
        vm.buttons = [{
            title: 'Add Category',
            callback: goToAddCategoryPage
        }];

        vm.delete = ProductsService.deleteCategory;

        categoriesToPlain(vm.categories,vm.plainCategories,0);
        vm.total = vm.plainCategories.length;

        function categoriesToPlain(categories,plainCategories,level){
            angular.forEach(categories,function(value, key) {
                var pre = '';
                var i=1;
                if(level>0) while(i<=level){
                    pre = pre+'- ';
                    i++;
                }
                var parent = {
                    name: pre+value.name,
                    id: value.id
                };
                if(value.parentId){
                    parent.parentId = value.parentId;
                }
                plainCategories.push(parent);
                if(value.subcategories){
                    var nLevel = level+1;
                    categoriesToPlain(value.subcategories,plainCategories,nLevel);
                }
            });
        }

        function goToAddCategoryPage(){
            $state.go('index.products.categories.addnew');
        }

    }
})();
