(function() {
    'use strict';

    angular.module('speedspa.products')
        .controller('CategoryCtrl', CategoryCtrl);

    CategoryCtrl.$inject = [
        'ProductsService',
        '$state',
        'productsGetCategoryResolve',
        'productsCategoriesResolve'
    ];

    function CategoryCtrl(
        ProductsService,
        $state,
        productsGetCategoryResolve,
        productsCategoriesResolve
    ) {

        var vm = this;

        vm.title = $state.current.data.bcName;
        vm.category = productsGetCategoryResolve;
        vm.categories = productsCategoriesResolve;
        vm.plainCategories = [];
        vm.errors = '';
        vm.dataWasSaved = false;
        vm.addnew = true;
        vm.saving = false;

        vm.save = save;
        vm.cancel = cancel;

        if($state.params.categoryId) {
            vm.addnew = false;
        }

        categoriesToPlain(vm.categories,vm.plainCategories,0);

        function categoriesToPlain(categories,plainCategories,level){
            angular.forEach(categories,function(value, key) {
                var pre = '';
                var i=1;
                if(level>0) while(i<=level){
                    pre = pre+'- ';
                    i++;
                }
                var parent = {
                    name: pre+value.name,
                    id: value.id
                };
                if(value.parentId){
                    parent.parentId = value.parentId;
                }
                plainCategories.push(parent);
                if(value.subcategories){
                    var nLevel = level+1;
                    categoriesToPlain(value.subcategories,plainCategories,nLevel);
                }
            });
        }

        function save(){
            vm.dataWasSaved = true;

            vm.saving = true;
            if(!vm.category.parentId) delete vm.category.parentId;
            if(vm.addnew) add();
            else update();
        }

        function add(){
            if (checkData()) ProductsService.addCategory(vm.category).then(function(res) {
                vm.category = res;
                vm.saving = false;
                $state.go('index.products.categories.all');
            }).catch(function(err) {
                console.log(err, 'error');
                vm.errors = 'Can not save your data. '+err.data.message;
                vm.saving = false;
            });
            else {
                vm.errors = 'Please fill all required fields';
                vm.saving = false;
            }
        }

        function update(){
            if (checkData()) ProductsService.updateCategory(vm.category).then(function(res) {
                vm.category = res;
                vm.saving = false;
                $state.go('index.products.categories.all');
            }).catch(function(err) {
                console.log(err, 'error');
                vm.errors = 'Can not save your data. '+err.data.message;
                vm.saving = false;
            });
            else {
                vm.errors = 'Please fill all required fields';
                vm.saving = false;
            }
        }

        function cancel(){
            vm.saving = false;
            $state.go('index.products.categories.all');
        }

        function checkData(){
            if(
                ( vm.category.name && vm.category.name.length )
            ) return true;
            else return false;
        }

    }
})();
