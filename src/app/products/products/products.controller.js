(function() {
    'use strict';

    angular.module('speedspa.products')
        .controller('ProductsCtrl', ProductsCtrl);

    ProductsCtrl.$inject = [
        'productsResolve',
        'ProductsService',
        'QueryDataFilterService',
        '$state',
        '$scope',
        '$filter'
    ];

    function ProductsCtrl(
        productsResolve,
        ProductsService,
        QueryDataFilterService,
        $state,
        $scope,
        $filter
    ) {

        var vm = this;

        vm.products = productsResolve;
        vm.total = vm.products.length;
        vm.currentPage = 1;
        vm.itemsPerPage = 10;
        vm.searchQuery = {
            text: '',
            status: '',
            pricecondition: 'more',
            price: '',
            quantitycondition: 'more',
            quantity: ''
        };
        vm.orderby = {
            field: '',
            desc: false
        };
        vm.buttons = [{
            title: 'Add Product',
            callback: goToAddProductPage
        }];

        vm.search = search;
        vm.reset = reset;
        vm.order = order;
        vm.delete = ProductsService.deleteProduct;

        order('name');

        function search(){
            var data = {};
            if(vm.searchQuery.text && vm.searchQuery.text.length){
                data.name = {
                    '$regex': vm.searchQuery.text,
                    '$options': 'i'
                };
            }
            if(vm.searchQuery.status && vm.searchQuery.status.length){
                if(vm.searchQuery.status == 'active')
                    data.active = {
                        '$eq':true
                    };
                if(vm.searchQuery.status == 'inactive')
                    data.active = {
                        '$eq':false
                    };
            }
            if(vm.searchQuery.price>0){
                if(vm.searchQuery.pricecondition == 'more')
                    data.price = {'$gte': (vm.searchQuery.price*100)};
                if(vm.searchQuery.pricecondition == 'less')
                    data.price = {'$lte': (vm.searchQuery.price*100)};
            }
            if(vm.searchQuery.quantity>0){
                if(vm.searchQuery.quantitycondition == 'more')
                    data.quantity = {'$gte': vm.searchQuery.quantity};
                if(vm.searchQuery.quantitycondition == 'less')
                    data.quantity = {'$lte': vm.searchQuery.quantity};
            }
            var query = QueryDataFilterService.buildFilter(data);

            $scope.$emit('loadingStart', 'Loading data was started!');
            ProductsService.getProducts(query).then(function(res) {
                vm.products = res.data? res.data:[];
                vm.total = vm.products.length;
                vm.orderby.field = '';
                order('name');
                $scope.$emit('loadingEnd', 'Loading data was ended!');
            }).catch(function(err) {
                console.log(err, 'error');
                vm.products = [];
                vm.total = 0;
                $scope.$emit('loadingEnd', 'Loading data was ended!');
            })
        }

        function reset(){
            vm.searchQuery = {
                text: '',
                status: '',
                pricecondition: 'more',
                price: '',
                quantitycondition: 'more',
                quantity: ''
            };
            search();
        }

        function order(by){
            var oldBy = angular.copy(vm.orderby.field);
            if(by == oldBy){
                vm.orderby.desc = !vm.orderby.desc;
            }
            else {
                vm.orderby.field = by;
                vm.orderby.desc = false;
            }

            vm.products = $filter('orderBy')(vm.products, vm.orderby.field, vm.orderby.desc);

            vm.currentPage = 1;
        }

        function goToAddProductPage(){
            $state.go('index.products.addnew');
        }

    }
})();
