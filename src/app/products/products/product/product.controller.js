(function() {
    'use strict';

    angular.module('speedspa.products')
        .controller('ProductCtrl', ProductCtrl);

    ProductCtrl.$inject = [
        'productsGetProductResolve',
        'productsCategoriesResolve',
        'ProductsService',
        '$state',
        '$scope'
    ];

    function ProductCtrl(
        productsGetProductResolve,
        productsCategoriesResolve,
        ProductsService,
        $state,
        scope
    ) {

        var vm = this;

        vm.product = productsGetProductResolve;
        vm.title = $state.current.data.bcName;
        vm.categories = productsCategoriesResolve;
        vm.categoriesData = [];
        vm.productCategories = [];
        vm.errors = '';
        vm.dataWasSaved = false;
        vm.addnew = true;
        vm.showTempImage = false;
        vm.tempImage = null;
        vm.imagesToShow = [];
        vm.saving = false;
        vm.uploading = false;

        vm.save = save;
        vm.cancel = cancel;
        vm.uploadImage = uploadImage;
        vm.removeImage = removeImage;
        vm.MultiSelectTreeCallback = MultiSelectTreeCallback;

        normalizeProduct();
        vm.categoriesData = categoriesToData(vm.categories,0);
        if($state.params.productId) {
            vm.addnew = false;
        }

        function categoriesToData(categories,level){
            var data = _.chain(categories).map(function(value, key) {
                if(vm.product.categoriesIds && vm.product.categoriesIds.length>0){
                    if(_.find(vm.product.categoriesIds, function(id) { return id == value.id; })){
                        value.selected = true;
                    }
                    else {
                        value.selected = false;
                    }
                }
                else {
                    value.selected = false;
                }
                if(!value.subcategories) value.children = [];
                return  _.transform(value, function(result, value, key) {
                    if (key === 'subcategories') {
                        var nLevel = level + 1;
                        result['children'] = categoriesToData(value,nLevel);
                    }
                    else{
                        result[key] = value;
                    }
                });
            }).value();
            return data;
        }

        function MultiSelectTreeCallback(item, selectedItems) {
            if (selectedItems !== undefined && selectedItems.length >= 80) {
                return false;
            } else {
                return true;
            }
        }

        function normalizeProduct(){
            vm.imagesToShow = _.map(vm.product.images, function(item, index){
                return {
                    url: item,
                    index: index
                };
            });
            if(!vm.product.quantity) vm.product.quantity = 1;
            if(!vm.product.minQuantity) vm.product.minQuantity = 0;
            if(!vm.product.sortOrder) vm.product.sortOrder = 0;
            vm.product.price = vm.product.price/100;
        }

        function save(){
            vm.dataWasSaved = true;
            vm.product.price = vm.product.price*100;
            vm.product.images = _.sortBy(vm.imagesToShow, ['index','url'], ['asc', 'desc']).map(function(item){
                return item.url;
            });
            vm.product.categoriesIds = [];
            if (vm.productCategories.length >0) {
                angular.forEach(vm.productCategories, function(value,key){
                    vm.product.categoriesIds.push(value.id);
                });
            }

            vm.saving = true;
            if(vm.addnew) add();
            else update();
        }

        function add(){
            if (checkData())
                ProductsService.addProduct(vm.product).then(function(res) {
                    vm.product = res;
                    normalizeProduct();
                    vm.saving = false;
                    $state.go('index.products.products');
                }).catch(function(err) {
                    console.log(err, 'error');
                    vm.errors = 'Can not save your data. '+err.data.message;
                    vm.saving = false;
                });
            else {
                    vm.errors = 'Please fill all required fields';
                    normalizeProduct();
                    vm.saving = false;
                }
        }

        function update(){
            if (checkData())
                ProductsService.updateProduct(vm.product).then(function(res) {
                    vm.product = res;
                    normalizeProduct();
                    vm.saving = false;
                    $state.go('index.products.products');
                }).catch(function(err) {
                    console.log(err, 'error');
                    vm.errors = 'Can not save your data. '+err.data.message;
                    vm.saving = false;
                });
            else {
                    vm.errors = 'Please fill all required fields';
                    normalizeProduct();
                    vm.saving = false;
                }
        }

        function cancel(){
            vm.saving = false;
            $state.go('index.products.products');
        }

        scope.setFile = function(element) {
            scope.$apply(function() {
                vm.tempImage = element.files[0];
            });
        };

        function uploadImage(){
            var imageToSave = new FormData();
            imageToSave.append("input", vm.tempImage);
            vm.uploading = true;
            ProductsService.uploadImageToProduct(imageToSave).then(function(res) {
                vm.imagesToShow.push({
                    url: res.url,
                    index: vm.product.images.length
                });
                vm.product.images.push(res.url);
                vm.showTempImage = false;
                vm.uploading = false;
            }).catch(function(err) {
                console.log(err, 'error');
                vm.errors = 'Can not save your data. '+err.data.message;
                vm.uploading = false;
            });
        }

        function removeImage(index){
            vm.imagesToShow.splice(index,1);
            vm.product.images.splice(index,1);
        }

        function checkData(){
            if(
                (vm.product.name && vm.product.name.length >0 )
                && ( vm.product.caption && vm.product.caption.length >0 )
                && ( vm.product.description && vm.product.description.length >0 )
                && ( vm.product.active!=='undefined' )
                && ( vm.product.images )
                && ( vm.product.price && vm.product.price >0 )
                && ( vm.product.quantity && vm.product.quantity >0 )
                && ( vm.product.minQuantity!=='undefined' && vm.product.minQuantity >=0 )
                && ( vm.product.quantity >= vm.product.minQuantity )
                && ( vm.product.sortOrder!=='undefined' && vm.product.sortOrder >=0 )
                && ( vm.productCategories.length >0 )
            ) return true;
            else return false;
        }

    }
})();
