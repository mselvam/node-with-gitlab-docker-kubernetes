(function() {
    'use strict';
    angular
        .module('speedspa.products')
        .factory('ProductsService', ProductsService);

    ProductsService.$inject = [
        'Restangular',
        'UploadImagesRestangular',
        '$q'
    ];

    function ProductsService(
        Restangular,
        UploadImagesRestangular,
        $q
    ) {
        var service = {
            getProducts: getProducts,
            getProduct: getProduct,
            addProduct: addProduct,
            updateProduct: updateProduct,
            deleteProduct: deleteProduct,
            uploadImageToProduct: uploadImageToProduct,
            getOrders: getOrders,
            getOrdersReport: getOrdersReport,
            getOrder: getOrder,
            getOrderReport: getOrderReport,
            addOrder: addOrder,
            updateOrder: updateOrder,
            deleteOrder: deleteOrder,
            getCategories: getCategories,
            getCategory: getCategory,
            addCategory: addCategory,
            updateCategory: updateCategory,
            deleteCategory: deleteCategory
        };

        return service;

        function getProducts(query){
            return Restangular
                .one('products')
                .get(query)
                .then(getProductsComplete)
                .catch(getProductsFailed);

            function getProductsComplete(res) {
                return res.plain();
            }

            function getProductsFailed(err) {
                return $q.reject(err);
            }
        }

        function getProduct(id){
            return Restangular
                .all('products')
                .get(id)
                .then(getProductComplete)
                .catch(getProductFailed);

            function getProductComplete(res) {
                return res.plain();
            }

            function getProductFailed(err) {
                return $q.reject(err);
            }
        }

        function addProduct(product){
            return Restangular
                .all('products')
                .post(product)
                .then(addProductComplete)
                .catch(addProductFailed);

            function addProductComplete(res) {
                return res.plain();
            }

            function addProductFailed(err) {
                return $q.reject(err);
            }
        }

        function updateProduct(product){
            return Restangular
                .one('products', product.id)
                .patch(product)
                .then(updateProductComplete)
                .catch(updateProductFailed);

            function updateProductComplete(res) {
                return res.plain();
            }

            function updateProductFailed(err) {
                return $q.reject(err);
            }
        }

        function deleteProduct(product){
            return Restangular
                .one('products', product.id)
                .remove()
                .then(deleteProductComplete)
                .catch(deleteProductFailed);

            function deleteProductComplete(res) {
                return res.plain();
            }

            function deleteProductFailed(err) {
                return $q.reject(err);
            }
        }

        function uploadImageToProduct(image){
            return UploadImagesRestangular
                .one('s3/upload')
                .withHttpConfig({
                    transformRequest: angular.identity
                })
                .customPOST(image, undefined, undefined, {'Content-Type': function(){ return undefined; }})
                .then(uploadImageToProductComplete)
                .catch(uploadImageToProductFailed);

            function uploadImageToProductComplete(res) {
                return res.plain();
            }

            function uploadImageToProductFailed(err) {
                return $q.reject(err);
            }
        }

        function getOrders(query){
            if(query) query['type[$eq]']='products';
            else var query = {
                'type[$eq]':'products'
            };
            return Restangular
                .one('orders')
                .get(query)
                .then(getOrdersComplete)
                .catch(getOrdersFailed);

            function getOrdersComplete(res) {
                return res.plain();
            }

            function getOrdersFailed(err) {
                return $q.reject(err);
            }
        }

        function getOrdersReport(query){
            if(query) query['type[$eq]']='products';
            else var query = {
                'type[$eq]':'products'
            };
            return Restangular
                .one('reports/orders')
                .get(query)
                .then(getOrdersReportComplete)
                .catch(getOrdersReportFailed);

            function getOrdersReportComplete(res) {
                return res.plain();
            }

            function getOrdersReportFailed(err) {
                return $q.reject(err);
            }
        }

        function getOrder(id){
            return Restangular
                .one('orders',id)
                .get()
                .then(getOrderComplete)
                .catch(getOrderFailed);

            function getOrderComplete(res) {
                return res.plain();
            }

            function getOrderFailed(err) {
                return $q.reject(err);
            }
        }

        function getOrderReport(id){
            return Restangular
                .one('reports/orders',id)
                .get()
                .then(getOrderReportComplete)
                .catch(getOrderReportFailed);

            function getOrderReportComplete(res) {
                return res.plain();
            }

            function getOrderReportFailed(err) {
                return $q.reject(err);
            }
        }

        function addOrder(order){
            return Restangular
                .one('orders')
                .post('',order)
                .then(addOrderComplete)
                .catch(addOrderFailed);

            function addOrderComplete(res) {
                return res.plain();
            }

            function addOrderFailed(err) {
                return $q.reject(err);
            }
        }

        function updateOrder(order){
            return Restangular
                .one('orders', order.id)
                .patch(order)
                .then(updateOrderComplete)
                .catch(updateOrderFailed);

            function updateOrderComplete(res) {
                return res.plain();
            }

            function updateOrderFailed(err) {
                return $q.reject(err);
            }
        }

        function deleteOrder(order){
            return Restangular
                .one('orders', order.id)
                .remove()
                .then(deleteOrderComplete)
                .catch(deleteOrderFailed);

            function deleteOrderComplete(res) {
                return res.plain();
            }

            function deleteOrderFailed(err) {
                return $q.reject(err);
            }
        }

        function getCategories(query){
            return Restangular
                .one('products_categories')
                .get(query)
                .then(getCategoriesComplete)
                .catch(getCategoriesFailed);

            function getCategoriesComplete(res) {
                return res.plain();
            }

            function getCategoriesFailed(err) {
                return $q.reject(err);
            }
        }

        function getCategory(id){
            return Restangular
                .all('products_categories')
                .get(id)
                .then(getCategoryComplete)
                .catch(getCategoryFailed);

            function getCategoryComplete(res) {
                return res.plain();
            }

            function getCategoryFailed(err) {
                return $q.reject(err);
            }
        }

        function addCategory(category){
            return Restangular
                .all('products_categories')
                .post(category)
                .then(addCategoryComplete)
                .catch(addCategoryFailed);

            function addCategoryComplete(res) {
                return res.plain();
            }

            function addCategoryFailed(err) {
                return $q.reject(err);
            }
        }

        function updateCategory(category){
            return Restangular
                .one('products_categories', category.id)
                .customPUT(category) //   patch
                .then(updateCategoryComplete)
                .catch(updateCategoryFailed);

            function updateCategoryComplete(res) {
                return res.plain();
            }

            function updateCategoryFailed(err) {
                return $q.reject(err);
            }
        }

        function deleteCategory(category){
            return Restangular
                .one('products_categories', category.id)
                .remove()
                .then(deleteCategoryComplete)
                .catch(deleteCategoryFailed);

            function deleteCategoryComplete(res) {
                return res.plain();
            }

            function deleteCategoryFailed(err) {
                return $q.reject(err);
            }
        }
    }

})();
