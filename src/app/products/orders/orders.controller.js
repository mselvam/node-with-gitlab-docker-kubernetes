(function() {
    'use strict';

    angular.module('speedspa.products')
        .controller('OrdersCtrl', OrdersCtrl);

    OrdersCtrl.$inject = [
        'productsOrdersResolve',
        'clientsResolve',
        'ProductsService',
        'QueryDataFilterService',
        '$timeout',
        '$state',
        '$scope',
        '$filter'
    ];

    function OrdersCtrl(
        productsOrdersResolve,
        clientsResolve,
        ProductsService,
        QueryDataFilterService,
        $timeout,
        $state,
        $scope,
        $filter
    ) {

        var vm = this;

        vm.orders = productsOrdersResolve;
        vm.filteredOrders = vm.orders;
        vm.clients = clientsResolve;
        vm.total = vm.filteredOrders.length;
        vm.currentPage = 1;
        vm.itemsPerPage = 10;
        vm.popupDateAdded = {
            opened: false,
            dateOptions: {
                formatYear: 'yy',
                startingDay: 1
            }
        };
        vm.searchQuery = {
            status: '',
            client: '',
            dateAdded: new Date()
        };
        vm.orderby = {
            field: '',
            desc: true
        };
        vm.buttons = [{
            title: 'Add Order',
            callback: goToAddOrderPage
        }];

        vm.openDateAdded = openDateAdded;
        vm.search = search;
        vm.reset = reset;
        vm.order = order;
        vm.delete = ProductsService.deleteOrder;
        vm.summary = summary;

        search();
        order('created');

        function openDateAdded(){
            vm.popupDateAdded.opened = true;
        }

        function search(){

            var query = {};
            if(vm.searchQuery.status && vm.searchQuery.status.length){
                query.status = vm.searchQuery.status;
            }
            if(vm.searchQuery.client && vm.searchQuery.client.length){
                query.clientId = vm.searchQuery.client;
            }

            vm.filteredOrders = $filter('filter')(vm.orders, query, 'strict');

            if(vm.searchQuery.dateAdded){
                var start =  new Date(moment(vm.searchQuery.dateAdded).startOf('day'));
                var end =  new Date(moment(vm.searchQuery.dateAdded).endOf('day'));
                vm.filteredOrders = $filter('filter')(vm.filteredOrders, moreLessFilter('created', 'moreless', start, end));
            }

            vm.total = vm.filteredOrders.length;
            vm.orderby.desc = false;
            order('created');
        }

        function reset(){
            vm.searchQuery.status = '';
            vm.searchQuery.client = '';
            $timeout(function(){angular.element('#clientSelect2').select2().val(null).trigger("change");}, 100);
            search();
        }

        function order(by){
            var oldBy = angular.copy(vm.orderby.field);
            if(by == oldBy){
                vm.orderby.desc = !vm.orderby.desc;
            }
            else {
                vm.orderby.field = by;
                vm.orderby.desc = true;
            }

            vm.filteredOrders = $filter('orderBy')(vm.filteredOrders, vm.orderby.field, vm.orderby.desc);

            vm.currentPage = 1;
        }

        function moreLessFilter(prop, moreless, val1, val2){
            return function(item){
                if (moreless == 'more')
                    return item[prop] >= val1;
                else if (moreless == 'less')
                    return item[prop] <= val1;
                else if (moreless == 'moreless'){
                    return (item[prop] >= val1 && item[prop] <= val2);
                }
                else
                    return true;
            }
        }

        function summary(prop){
            var summary = 0;
            var props = prop.split('.');
            function returnProp(value,prop){return value[prop]}

            angular.forEach(vm.filteredOrders, function(value,key){
                var addToSum = value[props[0]];
                if(props.length>1){
                    for(var i=1;i<props.length;i++){
                        addToSum = returnProp(addToSum,props[i]);
                    }
                }
                summary = summary + addToSum;
            });
            return summary;
        }

        function goToAddOrderPage(){
            $state.go('index.products.orders.addnew');
        }

    }
})();
