(function() {
    'use strict';

    angular.module('speedspa.products')
        .controller('OrderCtrl', OrderCtrl);

    OrderCtrl.$inject = [
        'productsGetOrderResolve',
        'clientsResolve',
        'productsResolve',
        'ProductsService',
        '$state',
        '$timeout'
    ];

    function OrderCtrl(
        productsGetOrderResolve,
        clientsResolve,
        productsResolve,
        ProductsService,
        $state,
        $timeout
    ) {

        var vm = this;

        vm.order = productsGetOrderResolve;
        vm.products = productsResolve;
        vm.clients = clientsResolve;
        vm.title = $state.current.data.bcName;
        vm.errors = '';
        vm.dataWasSaved = false;
        vm.statuses = [
            'canceled',
            'returned',
            'fulfilled'
        ];
        vm.orderItem = {
            amount: 0,
            product: null,
            quantity: 0
        };
        vm.showTempItem = false;
        vm.addnew = true;
        vm.saving = false;

        vm.save = save;
        vm.cancel = cancel;
        vm.addToOrder = addToOrder;
        vm.removeItem = removeItem;
        vm.findProductById = findProductById;
        vm.subTotal = subTotal;

        if($state.params.orderId) {
            vm.addnew = false;
        }

        if( !(_.includes(vm.statuses, vm.order.status)) ){
            vm.statuses.push(vm.order.status);
        }
        angular.forEach(vm.order.items, function(value,key){
            value.amount = value.amount/100;
        });

        if(!vm.addnew){
            vm.order.created = new Date(vm.order.created*1000);
            vm.order.updated = new Date(vm.order.updated*1000);
        }

        function save(){
            vm.dataWasSaved = true;
            angular.forEach(vm.order.items, function(value,key){
                value.amount = value.amount*100;
            });

            vm.saving = true;
            if(vm.addnew) add();
            else update();
        }

        function add(){
            if (checkData())
                ProductsService.addOrder(vm.order).then(function(res) {
                    vm.order = res;
                    vm.saving = false;
                    $state.go('index.products.orders.all');
                }).catch(function(err) {
                    console.log(err, 'error');
                    vm.errors = 'Can not save your data. '+err.data.message;
                    vm.saving = false;
                });
                else {
                    vm.errors = 'Please fill all required fields';
                    vm.saving = false;
                }
        }

        function update(){
            if (checkData()){
                var order = {
                    id: vm.order.id,
                    items: vm.order.items,
                    clientId: vm.order.clientId
                };
                ProductsService.updateOrder(order).then(function(res) {
                    vm.order = res;
                    vm.saving = false;
                    $state.go('index.products.orders.all');
                }).catch(function(err) {
                    console.log(err, 'error');
                    vm.errors = 'Can not save your data. '+err.data.message;
                    vm.saving = false;
                });
            }
            else {
                vm.errors = 'Please fill all required fields';
                vm.saving = false;
            }
        }

        function cancel(){
            vm.saving = false;
            $state.go('index.products.orders.all');
        }

        function checkData(){
            if(
                (vm.order.clientId && vm.order.clientId.length >0 )
                && (vm.order.items && vm.order.items.length >0 )
            ) return true;
            else return false;
        }

        function addToOrder(){
            if(vm.orderItem.product && vm.orderItem.quantity>0){
                var item = {
                    parent: vm.orderItem.product.id,
                    description: vm.orderItem.product.name,
                    quantity: vm.orderItem.quantity,
                    amount: vm.orderItem.quantity*vm.orderItem.product.price/100
                };
                vm.order.items.push(item);
                vm.orderItem = {
                    amount: 0,
                    product: null,
                    quantity: 0
                };
                $timeout(function(){angular.element('#orderItemParent').select2().val(null).trigger("change");}, 100);
                vm.showTempItem = false;
            }
        }

        function removeItem(index){
            vm.order.items.splice(index,1);
        }

        function findProductById(id){
            return _.find(vm.products, {id:id});
        }

        function subTotal(){
            var sum = 0;
            angular.forEach(vm.order.items, function(value, key){
                sum = sum + (findProductById(value.parent)).price*value.quantity;
            });
            return sum;
        }

    }
})();
