(function() {
    'use strict';
    angular
        .module('speedspa.core')
        .factory('SessionService', SessionService);

    SessionService.$inject = [
        '$localStorage',
        '$state'
    ];

    function SessionService(
        $localStorage,
        $state
    ) {
        var service = {
            sessionCreate: sessionCreate,
            sessionDestroy: sessionDestroy,
            sessionCheck: sessionCheck
        };
        return service;

        function sessionCreate(user, token){
            $localStorage.user = user;
            $localStorage.token = token;
            return true;
        }

        function sessionDestroy(){
            delete $localStorage.user;
            delete $localStorage.token;
            $state.go('auth.login');
            return true;
        }

        function sessionCheck(){
            if( (!!$localStorage.user) && (!!$localStorage.token) )
                return true;
            else
                return false;
        }
    }

})();
