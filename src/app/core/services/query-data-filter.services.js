(function() {
    'use strict';
    angular
        .module('speedspa.core')
        .factory('QueryDataFilterService', QueryDataFilterService);

    QueryDataFilterService.$inject = [
    ];

    function QueryDataFilterService(
    ) {
        var service = {
            buildFilter: buildFilter
        };

        return service;

        function buildFilter(data) {
            var qParams = {};

            for (var property in data) {
                var formattedData = formatData(data[property]);

                for (var subProp in formattedData) {
                    qParams[property + subProp] = formattedData[subProp];
                }
            }

            return qParams;
        }

        function formatData(data) {
            if (angular.isObject(data)) {
                var result = {};
                var newProperty = {};
                for (var property in data) {
                    var formattedData = formatData(data[property]);

                    if (angular.isObject(formattedData)) {
                        for (var subProperty in formattedData) {
                            newProperty = '[' + property + ']' + subProperty;
                            result[newProperty] = formattedData[subProperty];
                        }
                    } else {
                        newProperty = '[' + property + ']';
                        result[newProperty] = formattedData;
                    }
                }

                return result;
            } else {
                return data;
            }
        }
    }

})();
