(function() {
    'use strict';
    angular
        .module('speedspa.core')
        .factory('UploadImagesRestangular', UploadImagesRestangular);

    UploadImagesRestangular.$inject = [
        'Restangular',
        'siteConfigs'
    ];

    function UploadImagesRestangular(
        Restangular,
        siteConfigs
    ) {
        return Restangular.withConfig(function(RestangularConfigurer) {
            RestangularConfigurer.setBaseUrl(siteConfigs.uploadApi? siteConfigs.uploadApi: 'http://upload.staging.speedspa.tep.pw');
        });
    }

})();
