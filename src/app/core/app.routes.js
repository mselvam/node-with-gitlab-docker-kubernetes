(function() {
    'use strict';
    angular
        .module('speedspa.core', [
            'ui.router',
            'ngStorage',
            'angularUtils.directives.uiBreadcrumbs',
            'toastr'
        ])
        .config(routerConfig);

    routerConfig.$inject = [
        '$stateProvider',
        '$urlRouterProvider'
    ];

    function routerConfig(
        $stateProvider,
        $urlRouterProvider
    ) {

        $stateProvider
            .state('index', {
                abstract: true,
                templateUrl: "app/core/content.html",
                data: {
                    proxy: 'index.schedule.schedule',
                    permissions: ['stylist','admin']
                }
            })
            .state('index.404', {
                url: "/404",
                templateUrl: "app/core/404/404.html"
            });

        $urlRouterProvider.otherwise('/404');
    }

})();
