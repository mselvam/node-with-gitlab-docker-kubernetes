(function() {
  'use strict';

  angular.module('speedspa.core')
      .controller('MainCtrl', MainCtrl);

  MainCtrl.$inject = [
      '$localStorage',
      '$location',
      'Restangular',
      'SessionService',
      '$transitions',
      '$state',
      '$rootScope',
      '$scope',
      'toastr'
  ];

  function MainCtrl(
      $localStorage,
      $location,
      Restangular,
      SessionService,
      $transitions,
      $state,
      $rootScope,
      $scope,
      toastr
  ) {

    var vm = this;

    $rootScope.loading = false;

    $rootScope.$on('$locationChangeStart', onLocationChangeStart);

    $transitions.onBefore( {}, onStartTransition);

    $transitions.onSuccess( {}, onSuccessTransition);

    $scope.$on('loadingStart', onLoadingStart);

    $scope.$on('loadingEnd', onLoadingEnd);

    Restangular.addFullRequestInterceptor(fullRequestInterceptorFunc);

    Restangular.setErrorInterceptor(errorInterceptorFunc);

    function onLocationChangeStart(event, next, current) {
        if( ($location.path() != '/') && !SessionService.sessionCheck() ){
            $location.path('/');
        }
        else {
            if( ( ($location.path() == '/') || ($location.path() == '') ) && SessionService.sessionCheck() ){
                $location.path('/schedule');
            }
        }
    }

    function onStartTransition(trans){
        if(SessionService.sessionCheck()){
            if(_.includes($localStorage.user.roles, 'admin'))
                $rootScope.userRole = 'admin';
            else if (_.includes($localStorage.user.roles, 'stylist'))
                $rootScope.userRole = 'stylist';
        }

        if(
            ( ( (trans.$to().name).includes('auth') ) || (!$localStorage.user) )
            || (
                ($state.get(trans.$to().name))
                && ($state.get(trans.$to().name).data)
                && ($state.get(trans.$to().name).data.permissions)
                && (_.find($state.get(trans.$to().name).data.permissions, function(item){ return item == $localStorage.user.roles[0]; }))
            )
        ) {
            if( $state.current.name && trans.$to().name === $state.current.name ){}
            else {
                $scope.$emit('loadingStart', 'Loading data was started!');
            }
            return true;
        }
        else {
            $state.go('index.schedule.schedule');
            return false;
        }

    }

    function onSuccessTransition(){
        $scope.$emit('loadingEnd', 'Loading data was ended!');
    }

    function onLoadingStart(){
        $rootScope.loading = true;
    }

    function onLoadingEnd(){
        $rootScope.loading = false;
    }

    function fullRequestInterceptorFunc(element, operation, what, url, headers){
        if(SessionService.sessionCheck()){
            if( url.indexOf('timeslots') !== -1){
                Restangular.setDefaultHeaders({
                    'Content-Type': 'application/json',
                    'Authorization': $localStorage.token,
                    'x-populate': 'clientId,serviceId'
                });
            }
            else if( url.indexOf('orders') !== -1 ){
                Restangular.setDefaultHeaders({
                    'Content-Type': 'application/json',
                    'Authorization': $localStorage.token,
                    'x-populate': 'clientId,serviceId'
                });
            }
            else if( url.indexOf('appointments') !== -1 ){
                Restangular.setDefaultHeaders({
                    'Content-Type': 'application/json',
                    'Authorization': $localStorage.token,
                    'x-populate': 'clientId,resourceId'
                });
            }
            else if( (url.indexOf('services') !== -1) && (url.indexOf('linked') === -1) ){
                Restangular.setDefaultHeaders({
                    'Content-Type': 'application/json',
                    'Authorization': $localStorage.token,
                    'x-populate': 'categoryId'
                });
            }
            else if( url.indexOf('upload') !== -1){
                //do nothing
            }
            else {
                Restangular.setDefaultHeaders({
                    'Content-Type': 'application/json',
                    'Authorization': $localStorage.token
                });
            }
        }
        else {
            Restangular.setDefaultHeaders({
                'Content-Type': 'application/json'
            });
        }
        return true;
    }

    function errorInterceptorFunc(response, deferred, responseHandler) {

      if (response.status == 401) {
          console.log('interceptor: ', response.config.url);
          if((response.config.url).indexOf('auth/email') !== -1){
              SessionService.sessionDestroy();
              return true;
          } else {
              SessionService.sessionDestroy();
              return false;
          }
      }

      else if (response.status == 403) {
          SessionService.sessionDestroy();
          return true;
      }

      else if (response.status == 404) {
          $location.path('/404');
          toastr.error(response.data.message, 'Status: '+response.statusText);
          return true;
      }
      else if (response.status == 503) {
          toastr.error(response.data.message, 'Status: '+response.statusText);
          return true;
      }
      else if (response.status == 500) {
          toastr.error('Internal server error. Please come back later.', 'Status: Error');
          return true;
      }

      else {
          toastr.error(response.data.message, 'Status: '+response.statusText);
          return true;
      }

      return true;
    }

  }
})();
