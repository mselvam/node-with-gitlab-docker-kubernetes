(function() {
    'use strict';
    angular
        .module('speedspa.core')
        .directive('partNav', partNav);

    partNav.$inject = [
    ];

    function partNav(
    ) {
        var directive = {
            restrict: 'E',
            replace: true,
            templateUrl: 'app/core/components/nav/nav.html',
            scope: {
                sometext: '='
            },
            controller: partNavCtrl,
            controllerAs: 'vm',
            bindToController: true // because the scope is isolated
        };

        return directive;
    }



    partNavCtrl.$inject = [
        '$element',
        '$state'
    ];

    function partNavCtrl(
        $element,
        $state
    ) {

        var vm = this;

        vm.$state = $state;

        var navElementsWithSecondLevel = $element[0].getElementsByClassName('nav-second-level-label');

        angular.element(navElementsWithSecondLevel).bind('click', function (event) {
            event.preventDefault();
            angular.element(this).parent('li').toggleClass("slideddown");
            angular.element(this).next('ul').toggleClass("collapse");
        });

    }

})();