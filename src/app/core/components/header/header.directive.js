(function() {
    'use strict';
    angular
        .module('speedspa.core')
        .directive('partHeader', partHeader);

    partHeader.$inject = [
    ];

    function partHeader(
    ) {
        var directive = {
            restrict: 'E',
            replace: true,
            templateUrl: 'app/core/components/header/header.html',
            scope: {
                sometext: '='
            },
            controller: partHeaderCtrl,
            controllerAs: 'vm',
            bindToController: true // because the scope is isolated
        };

        return directive;
    }



    partHeaderCtrl.$inject = [
        '$element',
        'SessionService'
    ];

    function partHeaderCtrl(
        $element,
        SessionService
    ) {

        var vm = this;

        vm.logout = logout;

        var navbarMinimalize = $element[0].getElementsByClassName('navbar-minimalize')[0];
        var body = document.getElementsByTagName('body')[0];

        angular.element(navbarMinimalize).bind('click', function (event) {
            event.preventDefault();
            angular.element(body).toggleClass("mini-navbar");
        });

        function logout(){
            SessionService.sessionDestroy();
        }

    }

})();