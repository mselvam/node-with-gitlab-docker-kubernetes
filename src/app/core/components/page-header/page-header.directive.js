(function() {
    'use strict';
    angular
        .module('speedspa.core')
        .directive('partPageHeader', partPageHeader);

    partPageHeader.$inject = [
    ];

    function partPageHeader(
    ) {
        var directive = {
            restrict: 'E',
            replace: true,
            templateUrl: 'app/core/components/page-header/page-header.html',
            scope: {
                title: '=',
                buttons: '=' // [{title: 'title', callback: callback()}]
            },
            controller: partPageHeaderCtrl,
            controllerAs: 'vm',
            bindToController: true // because the scope is isolated
        };

        return directive;
    }

    partPageHeaderCtrl.$inject = [
        '$state'
    ];

    function partPageHeaderCtrl(
        $state
    ) {

        var vm = this;

        vm.$state = $state;

    }

})();