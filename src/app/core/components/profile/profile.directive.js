(function() {
    'use strict';
    angular
        .module('speedspa.core')
        .directive('partProfile', partProfile);

    partProfile.$inject = [
    ];

    function partProfile(
    ) {
        var directive = {
            restrict: 'E',
            replace: true,
            templateUrl: 'app/core/components/profile/profile.html',
            scope: {
                sometext: '='
            },
            controller: partProfileCtrl,
            controllerAs: 'vm',
            bindToController: true // because the scope is isolated
        };

        return directive;
    }



    partProfileCtrl.$inject = [
        'SessionService',
        '$localStorage'
    ];

    function partProfileCtrl(
        SessionService,
        $localStorage
    ) {

        var vm = this;

        vm.logout = logout;

        vm.user = $localStorage.user;

        function logout(){
            SessionService.sessionDestroy();
        }

    }

})();