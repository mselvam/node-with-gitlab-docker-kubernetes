(function() {
    'use strict';
    angular
        .module('speedspa.core')
        .directive('themeBehaviour', themeBehaviour);

    themeBehaviour.$inject = [
        '$window',
        '$timeout'
    ];

    function themeBehaviour(
        $window,
        $timeout
    ) {
        var directive = {
            restrict: 'A',
            link: link
        };

        return directive;

        function link(scope, elem, attrs) {

            $timeout(getMainSetting(), 300);

            scope.closePanel = closePanel;

            function getMainSetting(){

                var w = angular.element($window);

                if (elem.width() < 769) {
                    elem.addClass('body-small')
                } else {
                    elem.removeClass('body-small')
                }

                fix_height();

                w.bind('resize', function () {
                    if (elem.width() < 769) {
                        elem.addClass('body-small')
                    } else {
                        elem.removeClass('body-small')
                    }
                });

                w.bind('load resize scroll', function () {
                    if (!elem.hasClass('body-small')) {
                        fix_height();
                    }
                });

                function fix_height(){

                    var heightWithoutNavbar = elem.find('#wrapper').height() - 61;
                    elem.find('.sidebar-panel').css("min-height", heightWithoutNavbar + "px");

                    var navbarheight = elem.find('nav.navbar-default').height();
                    var wrapperHeight = elem.find('#page-wrapper').height();

                    if (navbarheight > wrapperHeight) {
                        elem.find('#page-wrapper').css("min-height", navbarheight + "px");
                    }

                    if (navbarheight < wrapperHeight) {
                        elem.find('#page-wrapper').css("min-height", $(window).height() + "px");
                    }

                    if (elem.hasClass('fixed-nav')) {
                        if (navbarheight > wrapperHeight) {
                            elem.find('#page-wrapper').css("min-height", navbarheight + "px");
                        } else {
                            elem.find('#page-wrapper').css("min-height", w.height() - 60 + "px");
                        }
                    }
                }

            }

            function closePanel($event){
                var ibox = angular.element($event.currentTarget).closest('.ibox');
                var button = angular.element($event.currentTarget).find('i');
                var content = ibox.children('.ibox-content');
                content.slideToggle(200);
                button.toggleClass('fa-chevron-up').toggleClass('fa-chevron-down');
                ibox.toggleClass('border-bottom');
                $timeout(function () {
                    ibox.resize();
                    ibox.find('[id^=map-]').resize();
                }, 50);
            }
        }
    }

})();