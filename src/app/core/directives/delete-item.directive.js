(function() {
    'use strict';

    angular.module('speedspa.core')
        .directive('deleteItemButton', deleteItemButton);

    deleteItemButton.$inject = [
        '$uibModal'
    ];

    function deleteItemButton(
        $uibModal
    ) {
        var directive = {
            restrict: 'A',
            scope: {
                item: '=',
                items: '=',
                itemCallback: '=',
                itemType: '@',
                itemName: '='
            },
            link: link
        };

        return directive;

        function link(scope, elem, attrs) {

            //angular.element(elem).on('click', deleteItem(scope.item));
            angular.element(elem).bind('click', function () {
                event.preventDefault();
                deleteItem(scope.item);
            });

            function deleteItem(item){

                var modalInstance = $uibModal.open({
                    ariaLabelledBy: 'modal-title',
                    ariaDescribedBy: 'modal-body',
                    templateUrl: 'app/core/directives/delete-item.html',
                    controller: DeleteItemCtrl,
                    controllerAs: 'vm',
                    scope: scope,
                    resolve: {
                        itemToDelete: function () {
                            return item;
                        },
                        itemCallback: function () {
                            return scope.itemCallback;
                        }
                    }
                });

                modalInstance.result.then(function (deletedItem) {
                    var deletedItem = deletedItem;
                    _.remove(scope.items, {
                        id: deletedItem.id
                    });
                }, function () {
                    console.log('Modal dismissed at: ' + new Date());
                });

            }

            function DeleteItemCtrl(
                $uibModalInstance,
                itemToDelete,
                itemCallback
            ) {

                var vm = this;

                vm.errors = '';
                vm.deleting = false;

                vm.delete = function () {
                    vm.deleting = true;
                    itemCallback(itemToDelete).then(function(res) {
                        vm.deleting = false;
                        $uibModalInstance.close(res);
                        vm.errors = '';
                    }).catch(function(err) {
                        vm.deleting = false;
                        console.log(err, 'error');
                        vm.errors = 'Can not delete this item. '+err.data.message;
                    });
                };

                vm.cancel = function () {
                    vm.deleting = false;
                    $uibModalInstance.dismiss('cancel');
                    vm.errors = '';
                };

            }

        }
    }
})();
