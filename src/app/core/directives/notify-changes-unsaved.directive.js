(function() {
    'use strict';
    angular
        .module('speedspa.core')
        .directive('notifyChangesUnsaved', notifyChangesUnsaved);

    notifyChangesUnsaved.$inject = [
        '$window',
        '$rootScope',
        '$uibModal',
        '$transitions'
    ];

    function notifyChangesUnsaved(
        $window,
        $rootScope,
        $uibModal,
        $transitions
    ) {
        var directive = {
            restrict: 'A',
            scope: {
                wassaved: '='
            },
            link: link
        };

        return directive;

        function link(scope, elem, attrs) {

            scope.userSayLeave = false;

            $transitions.onStart( {}, pauseLocationChange);

            angular.element($window).bind("beforeunload", showLosingDataMessage);

            function pauseLocationChange(trans) {

                if ( checkUnsavedData() && scope.userSayLeave === false && scope.wassaved === false ) {

                    var modalInstance;
                    modalInstance = $uibModal.open({
                        ariaLabelledBy: 'modal-title',
                        ariaDescribedBy: 'modal-body',
                        templateUrl: 'app/core/directives/notify-changes-unsaved.html',
                        controller: openModalCtrl,
                        controllerAs: 'vm'
                    });

                    return modalInstance.result.then(function (result) {
                        if (result == 'leave') {
                            scope.userSayLeave = true;
                            return true;
                        }
                    }, function (err) {
                        console.info('Modal dismissed at: ' + new Date());
                        return false;
                    });
                }

                openModalCtrl.$inject = [
                    '$uibModalInstance',
                    '$rootScope'
                ];

                function openModalCtrl(
                    $uibModalInstance,
                    $rootScope
                ) {

                    var vm = this;

                    vm.leave = function () {
                        $uibModalInstance.close('leave');
                    };
                    vm.stay = function () {
                        $rootScope.loading = false;
                        $uibModalInstance.dismiss('cancel');
                    };

                }

            }

            function showLosingDataMessage(e) {

                if (!(checkUnsavedData()) || (scope.userSayLeave === true) || (scope.wassaved === true) ) {
                    return undefined;
                }

                var confirmationMessage = 'All the data you did not save will be lost. Do you really want to leave without saving?';

                (e || $window.event).returnValue = confirmationMessage; //Gecko + IE
                return confirmationMessage; //Gecko + Webkit, Safari, Chrome etc.
            }

            function checkUnsavedData(){
                var dirtyFormInputs = _.map(
                    angular.element(elem).find('input')
                ).filter(function(item) {
                    return angular.element(item).hasClass('ng-dirty');
                });
                if (dirtyFormInputs.length) return true;
                else return false;
            }
        }
    }

})();