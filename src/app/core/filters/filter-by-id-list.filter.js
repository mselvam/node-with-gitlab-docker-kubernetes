(function() {
    'use strict';

    angular.module('speedspa.core')
        .filter('filterByIdList', filterByIdList);

    function filterByIdList() {
        return function (items, ids) {
            if(ids){
                var filtered = [];
                for (var i = 0; i < items.length; i++) {
                    var item = items[i];
                    if (
                        _.includes(ids, item.id)
                    ) {
                        filtered.push(item);
                    }
                }
                return filtered;
            }
            else {
                return items;
            }
        };
    }
})();
