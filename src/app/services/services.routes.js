(function() {
    'use strict';
    angular
        .module('speedspa.services', [
            'ui.router',
            'ui.bootstrap',
            'ui.select2'
        ])
        .config(routerConfig);

    routerConfig.$inject = [
        '$stateProvider'
    ];

    function routerConfig(
        $stateProvider
    ) {

        $stateProvider
            .state('index.services', {
                url: "/services",
                abstract: true,
                template: "<div data-ui-view></div>",
                data: {
                    proxy: 'index.services.services',
                    permissions: ['admin']
                }
            })
            .state('index.services.services', {
                url: "/",
                templateUrl: "app/services/services/services.html",
                controller: 'ServicesCtrl',
                controllerAs: 'vm',
                data: {
                    bcName: 'Services'
                },
                resolve: {
                    servicesResolve: servicesResolve,
                    servicesCategoriesResolve: servicesCategoriesResolve
                }
            })
            .state('index.services.addnew', {
                url: "/add-new",
                templateUrl: "app/services/services/service/service.html",
                controller: 'ServiceCtrl',
                controllerAs: 'vm',
                data: {
                    bcName: 'Add New Service'
                },
                resolve: {
                    servicesGetServiceResolve: servicesGetEmptyServiceResolve,
                    servicesCategoriesResolve: servicesCategoriesResolve,
                    servicesResolve: servicesResolve
                }
            })
            .state('index.services.view', {
                url: "/view/:serviceId",
                templateUrl: "app/services/services/service/service.html",
                controller: 'ServiceCtrl',
                controllerAs: 'vm',
                data: {
                    bcName: 'View/Edit Service'
                },
                resolve: {
                    servicesGetServiceResolve: servicesGetServiceResolve,
                    servicesCategoriesResolve: servicesCategoriesResolve,
                    servicesResolve: servicesResolve
                }
            })
            .state('index.services.orders', {
                url: "/orders",
                abstract: true,
                template: "<div data-ui-view></div>",
                data: {
                    proxy: 'index.services.orders.all'
                }
            })
            .state('index.services.orders.all', {
                url: "/",
                templateUrl: "app/services/orders/orders.html",
                controller: 'ServicesOrdersCtrl',
                controllerAs: 'vm',
                data: {
                    bcName: 'Orders'
                },
                resolve: {
                    servicesOrdersResolve: servicesOrdersResolve,
                    staffResolve: staffResolve
                }
            })
            .state('index.services.orders.view', {
                url: "/view/:orderId",
                templateUrl: "app/services/orders/order/order.html",
                controller: 'ServicesOrderCtrl',
                controllerAs: 'vm',
                data: {
                    bcName: 'Order View/Edit'
                },
                resolve: {
                    servicesGetOrderResolve: servicesGetOrderResolve,
                    clientsResolve: clientsResolve,
                    servicesResolve: servicesResolve
                }
            })
            .state('index.services.orders.addnew', {
                url: "/add-new",
                templateUrl: "app/services/orders/order/order.html",
                controller: 'ServicesOrderCtrl',
                controllerAs: 'vm',
                data: {
                    bcName: 'New Order'
                },
                resolve: {
                    servicesGetOrderResolve: servicesGetEmptyOrderResolve,
                    clientsResolve: clientsResolve,
                    servicesResolve: servicesResolve
                }
            })
            .state('index.services.categories', {
                url: "/categories",
                abstract: true,
                template: "<div data-ui-view></div>",
                data: {
                    proxy: 'index.services.categories.all'
                }
            })
            .state('index.services.categories.all', {
                url: "/",
                templateUrl: "app/services/categories/categories.html",
                controller: 'ServicesCategoriesCtrl',
                controllerAs: 'vm',
                data: {
                    bcName: 'Categories'
                },
                resolve: {
                    servicesCategoriesResolve: servicesCategoriesResolve
                }
            })
            .state('index.services.categories.addnew', {
                url: "/add-new",
                templateUrl: "app/services/categories/category/category.html",
                controller: 'ServicesCategoryCtrl',
                controllerAs: 'vm',
                data: {
                    bcName: 'New Category'
                },
                resolve: {
                    servicesGetCategoryResolve: servicesGetEmptyCategoryResolve
                }
            })
            .state('index.services.categories.view', {
                url: "/view/:categoryId",
                templateUrl: "app/services/categories/category/category.html",
                controller: 'ServicesCategoryCtrl',
                controllerAs: 'vm',
                data: {
                    bcName: 'Category View/Edit'
                },
                resolve: {
                    servicesGetCategoryResolve: servicesGetCategoryResolve
                }
            })
    }

    function servicesResolve(ServicesService, $q){
        return ServicesService.getServices().then(function(res) {
            return res;
        }).catch(function(err) {
            console.log(err, 'error');
            return [];
        });
    }

    function servicesCategoriesResolve(ServicesService, $q){
        return ServicesService.getCategories().then(function(res) {
            return res;
        }).catch(function(err) {
            console.log(err, 'error');
            return [];
        });
    }

    function servicesGetEmptyServiceResolve(){
        return {
            name: '',
            duration: [
                {
                    type: 'active',
                    time: 0
                },
                {
                    type: 'processing',
                    time: 0
                },
                {
                    type: 'active',
                    time: 0
                }
            ],
            links: {
                additional: []
            },
            price: 0,
            displayPrice: '',
            order: 0,
            topLevel: true,
            requiredAddOns: false,
            categoryId: null
        }
    }

    function servicesGetServiceResolve(ServicesService, $q, $stateParams){
        return ServicesService.getService($stateParams.serviceId).then(function(res) {
            return res;
        }).catch(function(err) {
            console.log(err, 'error');
            return {};
        });
    }

    function servicesGetEmptyCategoryResolve(){
        return {
            name: '',
            description: '',
            seats: 0
        }
    }

    function servicesGetCategoryResolve(ServicesService, $q, $stateParams){
        return ServicesService.getCategory($stateParams.categoryId).then(function(res) {
            return res;
        }).catch(function(err) {
            console.log(err, 'error');
            return {};
        });
    }

    function servicesOrdersResolve(ServicesService, ScheduleService, QueryDataFilterService, $q){
        var start = new Date(moment().add(1, 'day').startOf('day').subtract(1, 'milliseconds'));
        start.setTime( start.getTime() - start.getTimezoneOffset()*60*1000 );
        start = start.toISOString();
        var end = new Date(moment().add(1, 'day').endOf('day').add(1, 'milliseconds'));
        end.setTime( end.getTime() - end.getTimezoneOffset()*60*1000 );
        end = end.toISOString();
        var data = {
            appTimeStart: {
                $lte: end,
                $gt: start
            }
        };
        var query = QueryDataFilterService.buildFilter(data);
        return ServicesService.getOrdersReport().then(function(res) {
            var orders = (res.data && res.data.data)? res.data.data:[];
            var total = (res.data && res.data.total)? res.data.total:0;
            var limit = (res.data && res.data.limit)? res.data.limit:0;
            if(total>limit && limit>0) {
                var requestsNumber = Math.ceil(total/limit);
                var ordersPromise = Promise.resolve("cat");
                var ordersToGetListF = [];
                var numbers = _.range(1, requestsNumber);
                angular.forEach(numbers, function(value, key){
                    var get100 = ordersPromise.then(function () {
                        var query = {
                            '$skip':value*100
                        };
                        return ServicesService.getOrdersReport(query).then(function(skipRes) {
                            var skipOrders = (skipRes.data && skipRes.data.data)? skipRes.data.data:[];
                            orders = orders.concat(skipOrders);
                            return true;
                        }).catch(function(err) {
                            console.log(err, 'error');
                            return [];
                        });
                    });
                    ordersToGetListF.push(get100);
                });

                return $q.all(ordersToGetListF).then(function(){
                    angular.forEach(orders, function(value, key){
                        if(value.appTimeStart){
                            value.appTimeStart = new Date(value.appTimeStart);
                            value.appTimeStart = new Date(
                                value.appTimeStart.getUTCFullYear(),
                                value.appTimeStart.getUTCMonth(),
                                value.appTimeStart.getUTCDate() ,
                                value.appTimeStart.getUTCHours(),
                                value.appTimeStart.getUTCMinutes(),
                                value.appTimeStart.getUTCSeconds(),
                                value.appTimeStart.getUTCMilliseconds()
                            );
                        }
                        if(!(value.tip && value.tip.amount && value.tip.amount>0)){
                            value.tip = {
                                amount: 0
                            };
                        }
                        value.created = new Date(value.created*1000);
                        value.updated = new Date(value.updated*1000);
                        value.total = value.amount + value.tip.amount;
                    });
                    return orders;
                },function(err){
                    console.log(err, 'error');
                });
            }
            else {
                angular.forEach(orders, function(value, key){
                    if(value.appTimeStart){
                        value.appTimeStart = new Date(value.appTimeStart);
                        value.appTimeStart = new Date(
                            value.appTimeStart.getUTCFullYear(),
                            value.appTimeStart.getUTCMonth(),
                            value.appTimeStart.getUTCDate() ,
                            value.appTimeStart.getUTCHours(),
                            value.appTimeStart.getUTCMinutes(),
                            value.appTimeStart.getUTCSeconds(),
                            value.appTimeStart.getUTCMilliseconds()
                        );
                    }
                    if(!(value.tip && value.tip.amount && value.tip.amount>0)){
                        value.tip = {
                            amount: 0
                        };
                    }
                    value.created = new Date(value.created*1000);
                    value.updated = new Date(value.updated*1000);
                    value.total = value.amount + value.tip.amount;
                });
                return orders;
            }
        }).catch(function(err) {
            console.log(err, 'error');
            return [];
        });
    }

    function clientsResolve(ClientsService, $q){
        return ClientsService.getClients().then(function(res) {
            return res;
        }).catch(function(err) {
            console.log(err, 'error');
            return [];
        });
    }

    function staffResolve(StaffService, $q){
        return StaffService.getStaffList().then(function(res) {
            return res;
        }).catch(function(err) {
            console.log(err, 'error');
            return [];
        });
    }

    function servicesGetOrderResolve(ServicesService, $q, $stateParams){
        return ServicesService.getOrderReport($stateParams.orderId).then(function(res) {
            return res;
        }).catch(function(err) {
            console.log(err, 'error');
            return {};
        });
    }

    function servicesGetEmptyOrderResolve(){
        return {
            items: [],
            clientId: '',
            type: 'services'
        }
    }

})();
