(function() {
    'use strict';
    angular
        .module('speedspa.services')
        .factory('ServicesService', ServicesService);

    ServicesService.$inject = [
        'Restangular',
        '$q'
    ];

    function ServicesService(
        Restangular,
        $q
    ) {
        var service = {
            getServices: getServices,
            getServicesLinked: getServicesLinked,
            getCategories: getCategories,
            getService: getService,
            addService: addService,
            updateService: updateService,
            deleteService: deleteService,
            getCategory: getCategory,
            addCategory: addCategory,
            updateCategory: updateCategory,
            deleteCategory: deleteCategory,
            getOrders: getOrders,
            getOrdersReport: getOrdersReport,
            getOrder: getOrder,
            getOrderReport: getOrderReport,
            addOrder: addOrder,
            updateOrder: updateOrder,
            deleteOrder: deleteOrder
        };

        return service;

        function getServices(query){
            return Restangular
                .all('services')
                .getList(query)
                .then(getServicesComplete)
                .catch(getServicesFailed);

            function getServicesComplete(res) {
                return res.plain();
            }

            function getServicesFailed(err) {
                return $q.reject(err);
            }
        }

        function getServicesLinked(categoryId){
            var data = {};
            if(categoryId) {data.categoryId = categoryId;}
            return Restangular
                .all('services/linked')
                .getList(data)
                .then(getServicesLinkedComplete)
                .catch(getServicesLinkedFailed);

            function getServicesLinkedComplete(res) {
                return res.plain();
            }

            function getServicesLinkedFailed(err) {
                return $q.reject(err);
            }
        }

        function getCategories(){
            return Restangular
                .all('categories')
                .getList()
                .then(getCategoriesComplete)
                .catch(getCategoriesFailed);

            function getCategoriesComplete(res) {
                return res.plain();
            }

            function getCategoriesFailed(err) {
                return $q.reject(err);
            }
        }

        function getService(id){
            return Restangular
                .all('services')
                .get(id)
                .then(getServiceComplete)
                .catch(getServiceFailed);

            function getServiceComplete(res) {
                return res.plain();
            }

            function getServiceFailed(err) {
                return $q.reject(err);
            }
        }

        function addService(service){
            return Restangular
                .all('services')
                .post(service)
                .then(addServiceComplete)
                .catch(addServiceFailed);

            function addServiceComplete(res) {
                return res.plain();
            }

            function addServiceFailed(err) {
                return $q.reject(err);
            }
        }

        function updateService(service){
            return Restangular
                .one('services', service.id)
                .patch(service)
                .then(updateServiceComplete)
                .catch(updateServiceFailed);

            function updateServiceComplete(res) {
                return res.plain();
            }

            function updateServiceFailed(err) {
                return $q.reject(err);
            }
        }

        function deleteService(service){
            return Restangular
                .one('services', service.id)
                .remove()
                .then(deleteServiceComplete)
                .catch(deleteServiceFailed);

            function deleteServiceComplete(res) {
                return res.plain();
            }

            function deleteServiceFailed(err) {
                return $q.reject(err);
            }
        }

        function getCategory(id){
            return Restangular
                .all('categories')
                .get(id)
                .then(getCategoryComplete)
                .catch(getCategoryFailed);

            function getCategoryComplete(res) {
                return res.plain();
            }

            function getCategoryFailed(err) {
                return $q.reject(err);
            }
        }

        function addCategory(category){
            return Restangular
                .all('categories')
                .post(category)
                .then(addCategoryComplete)
                .catch(addCategoryFailed);

            function addCategoryComplete(res) {
                return res.plain();
            }

            function addCategoryFailed(err) {
                return $q.reject(err);
            }
        }

        function updateCategory(category){
            return Restangular
                .one('categories', category.id)
                .patch(category)
                .then(updateCategoryComplete)
                .catch(updateCategoryFailed);

            function updateCategoryComplete(res) {
                return res.plain();
            }

            function updateCategoryFailed(err) {
                return $q.reject(err);
            }
        }

        function deleteCategory(category){
            return Restangular
                .one('categories', category.id)
                .remove()
                .then(deleteCategoryComplete)
                .catch(deleteCategoryFailed);

            function deleteCategoryComplete(res) {
                return res.plain();
            }

            function deleteCategoryFailed(err) {
                return $q.reject(err);
            }
        }

        function getOrders(query){
            if(query) query['type[$eq]']='services';
            else var query = {
                'type[$eq]':'services'
            };
            return Restangular
                .one('orders')
                .get(query)
                .then(getOrdersComplete)
                .catch(getOrdersFailed);

            function getOrdersComplete(res) {
                return res.plain();
            }

            function getOrdersFailed(err) {
                return $q.reject(err);
            }
        }

        function getOrdersReport(query){
            if(query) query['type[$eq]']='services';
            else var query = {
                'type[$eq]':'services'
            };
            return Restangular
                .one('reports/orders')
                .get(query)
                .then(getOrdersReportComplete)
                .catch(getOrdersReportFailed);

            function getOrdersReportComplete(res) {
                return res.plain();
            }

            function getOrdersReportFailed(err) {
                return $q.reject(err);
            }
        }

        function getOrder(id){
            return Restangular
                .one('orders',id)
                .get()
                .then(getOrderComplete)
                .catch(getOrderFailed);

            function getOrderComplete(res) {
                return res.plain();
            }

            function getOrderFailed(err) {
                return $q.reject(err);
            }
        }

        function getOrderReport(id){
            return Restangular
                .one('reports/orders',id)
                .get()
                .then(getOrderReportComplete)
                .catch(getOrderReportFailed);

            function getOrderReportComplete(res) {
                return res.plain();
            }

            function getOrderReportFailed(err) {
                return $q.reject(err);
            }
        }

        function addOrder(order){
            return Restangular
                .one('orders')
                .post('',order)
                .then(addOrderComplete)
                .catch(addOrderFailed);

            function addOrderComplete(res) {
                return res.plain();
            }

            function addOrderFailed(err) {
                return $q.reject(err);
            }
        }

        function updateOrder(order){
            return Restangular
                .one('orders', order.id)
                .patch(order)
                .then(updateOrderComplete)
                .catch(updateOrderFailed);

            function updateOrderComplete(res) {
                return res.plain();
            }

            function updateOrderFailed(err) {
                return $q.reject(err);
            }
        }

        function deleteOrder(order){
            return Restangular
                .one('orders', order.id)
                .remove()
                .then(deleteOrderComplete)
                .catch(deleteOrderFailed);

            function deleteOrderComplete(res) {
                return res.plain();
            }

            function deleteOrderFailed(err) {
                return $q.reject(err);
            }
        }
    }

})();
