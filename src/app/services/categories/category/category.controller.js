(function() {
    'use strict';

    angular.module('speedspa.services')
        .controller('ServicesCategoryCtrl', ServicesCategoryCtrl);

    ServicesCategoryCtrl.$inject = [
        'ServicesService',
        '$state',
        'servicesGetCategoryResolve'
    ];

    function ServicesCategoryCtrl(
        ServicesService,
        $state,
        servicesGetCategoryResolve
    ) {

        var vm = this;

        vm.title = $state.current.data.bcName;
        vm.category = servicesGetCategoryResolve;
        vm.errors = '';
        vm.dataWasSaved = false;
        vm.addnew = true;
        vm.saving = false;

        vm.save = save;
        vm.cancel = cancel;

        if($state.params.categoryId) {
            vm.addnew = false;
        }

        function save(){
            vm.dataWasSaved = true;

            if( !(vm.category.resourcesIds && vm.category.resourcesIds.length>0) ) vm.category.resourcesIds = [];

            vm.saving = true;
            if(vm.addnew) add();
            else update();
        }

        function add(){
            if (checkData()) ServicesService.addCategory(vm.category).then(function(res) {
                vm.category = res;
                vm.saving = false;
                $state.go('index.services.categories.all');
            }).catch(function(err) {
                console.log(err, 'error');
                vm.errors = 'Can not save your data. '+err.data.message;
                vm.saving = false;
            });
            else {
                vm.errors = 'Please fill all required fields';
                vm.saving = false;
            }
        }

        function update(){
            if (checkData()) ServicesService.updateCategory(vm.category).then(function(res) {
                vm.category = res;
                vm.saving = false;
                $state.go('index.services.categories.all');
            }).catch(function(err) {
                console.log(err, 'error');
                vm.errors = 'Can not save your data. '+err.data.message;
                vm.saving = false;
            });
            else {
                vm.errors = 'Please fill all required fields';
                vm.saving = false;
            }
        }

        function cancel(){
            vm.saving = false;
            $state.go('index.services.categories.all');
        }

        function checkData(){
            if(
                ( vm.category.name && vm.category.name.length )
                && ( vm.category.description && vm.category.description.length )
                && ( vm.category.seats && vm.category.seats > 0 )
            ) return true;
            else return false;
        }

    }
})();
