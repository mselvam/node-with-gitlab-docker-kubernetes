(function() {
    'use strict';

    angular.module('speedspa.services')
        .controller('ServicesCategoriesCtrl', ServicesCategoriesCtrl);

    ServicesCategoriesCtrl.$inject = [
        'servicesCategoriesResolve',
        'ServicesService',
        '$state',
        '$filter'
    ];

    function ServicesCategoriesCtrl(
        servicesCategoriesResolve,
        ServicesService,
        $state,
        $filter
    ) {

        var vm = this;

        vm.categories = servicesCategoriesResolve;
        vm.total = vm.categories.length;
        vm.currentPage = 1;
        vm.itemsPerPage = 10;
        vm.orderby = {
            field: '',
            desc: false
        };
        vm.buttons = [{
            title: 'Add Category',
            callback: goToAddCategoryPage
        }];

        vm.order = order;
        vm.delete = ServicesService.deleteCategory;

        order('name');

        function order(by){
            var oldBy = angular.copy(vm.orderby.field);
            if(by == oldBy){
                vm.orderby.desc = !vm.orderby.desc;
            }
            else {
                vm.orderby.field = by;
                vm.orderby.desc = false;
            }

            vm.categories = $filter('orderBy')(vm.categories, vm.orderby.field, vm.orderby.desc);

            vm.currentPage = 1;
        }

        function goToAddCategoryPage(){
            $state.go('index.services.categories.addnew');
        }

    }
})();
