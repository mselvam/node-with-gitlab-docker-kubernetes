(function() {
    'use strict';

    angular.module('speedspa.services')
        .controller('ServicesCtrl', ServicesCtrl);

    ServicesCtrl.$inject = [
        'servicesResolve',
        'ServicesService',
        'servicesCategoriesResolve',
        'QueryDataFilterService',
        '$state',
        '$scope',
        '$filter'
    ];

    function ServicesCtrl(
        servicesResolve,
        ServicesService,
        servicesCategoriesResolve,
        QueryDataFilterService,
        $state,
        $scope,
        $filter
    ) {

        var vm = this;

        vm.services = servicesResolve;
        vm.categories = servicesCategoriesResolve;
        vm.total = vm.services.length;
        vm.currentPage = 1;
        vm.itemsPerPage = 10;
        vm.searchQuery = {
            text: '',
            category: null
        };
        vm.orderby = {
            field: '',
            desc: false
        };
        vm.buttons = [{
            title: 'Add Service',
            callback: goToAddServicePage
        }];

        vm.search = search;
        vm.reset = reset;
        vm.order = order;
        vm.delete = ServicesService.deleteService;

        order('name');

        function search(){
            var data = {};
            if(vm.searchQuery.text && vm.searchQuery.text.length){
                data.name = {
                    '$regex': vm.searchQuery.text,
                    '$options': 'i'
                }
            }
            if(vm.searchQuery.category && vm.searchQuery.category.length){
                data.categoryId = {
                    '$in': [vm.searchQuery.category]
                }
            }
            var query = QueryDataFilterService.buildFilter(data);

            $scope.$emit('loadingStart', 'Loading data was started!');
            ServicesService.getServices(query).then(function(res) {
                vm.services = res;
                vm.total = vm.services.length;
                vm.orderby.field = '';
                order('name');
                $scope.$emit('loadingEnd', 'Loading data was ended!');
            }).catch(function(err) {
                console.log(err, 'error');
                vm.services = [];
                vm.total = 0;
                $scope.$emit('loadingEnd', 'Loading data was ended!');
            })
        }

        function reset(){
            vm.searchQuery = {
                text: '',
                category: ''
            };
            search();
        }

        function order(by){
            var oldBy = angular.copy(vm.orderby.field);
            if(by == oldBy){
                vm.orderby.desc = !vm.orderby.desc;
            }
            else {
                vm.orderby.field = by;
                vm.orderby.desc = false;
            }

            vm.services = $filter('orderBy')(vm.services, vm.orderby.field, vm.orderby.desc);

            vm.currentPage = 1;
        }

        function goToAddServicePage(){
            $state.go('index.services.addnew');
        }

    }
})();
