(function() {
    'use strict';

    angular.module('speedspa.services')
        .controller('ServiceCtrl', ServiceCtrl);

    ServiceCtrl.$inject = [
        'ServicesService',
        '$state',
        'servicesGetServiceResolve',
        'servicesCategoriesResolve',
        'servicesResolve'
    ];

    function ServiceCtrl(
        ServicesService,
        $state,
        servicesGetServiceResolve,
        servicesCategoriesResolve,
        servicesResolve
    ) {

        var vm = this;

        vm.title = $state.current.data.bcName;
        vm.service = servicesGetServiceResolve;
        vm.categories = servicesCategoriesResolve;
        vm.services = servicesResolve;
        vm.errors = '';
        vm.dataWasSaved = false;
        vm.durationTemp = [
            {
                type: 'active',
                time: 0
            },
            {
                type: 'processing',
                time: 0
            },
            {
                type: 'active',
                time: 0
            }
        ];
        vm.linkedServices = [];

        vm.addLinkedService = addLinkedService;
        vm.deleteLinkedService = deleteLinkedService;
        vm.addLinkedOnOptionService = addLinkedOnOptionService;
        vm.deleteLinkedOnOptionService = deleteLinkedOnOptionService;
        vm.save = save;
        vm.cancel = cancel;
        vm.addnew = true;
        vm.saving = false;

        vm.service.price = vm.service.price/100;

        if($state.params.serviceId) {
            vm.addnew = false;
        }

        if( vm.service.duration && vm.service.duration.length < vm.durationTemp.length ){
            for( var i=(vm.service.duration.length); i<(vm.durationTemp.length); i++ ){
                vm.service.duration.push(vm.durationTemp[i]);
            }
        }
        if(!vm.service.duration){
            vm.service.duration = vm.durationTemp;
        }
        if(vm.service.topLevel === undefined){
            vm.service.topLevel = true;
        }
        if(vm.service.requiredAddOns === undefined){
            vm.service.requiredAddOns = false;
        }
        if(vm.service.links){
            if(vm.service.links.during && vm.service.links.during.length>0){
                angular.forEach(vm.service.links.during, function(value, key){
                    vm.linkedServices.push({
                        type: 'during',
                        serviceId: value,
                        servicesIds: []
                    });
                });
            }
            if(vm.service.links.addOns && vm.service.links.addOns.length>0){
                angular.forEach(vm.service.links.addOns, function(value, key){
                    var servicesIds = _.map(value.servicesIds, function(servicesId){
                        return {serviceId:servicesId};
                    });
                    vm.linkedServices.push({
                        type: 'addOns',
                        serviceId: value.serviceId,
                        servicesIds: servicesIds
                    });
                });
            }
            else {
                if(vm.service.links.additional && vm.service.links.additional.length>0){
                    angular.forEach(vm.service.links.additional, function(value, key){
                        vm.linkedServices.push({
                            type: 'addOns',
                            serviceId: value,
                            servicesIds: []
                        });
                    });
                }
                if(vm.service.links.onOption && vm.service.links.onOption.length>0){
                    angular.forEach(vm.service.links.onOption, function(value, key){
                        angular.forEach(vm.linkedServices, function(linkedService){
                            if(value.type == 'addOns')
                                linkedService.servicesIds.push({serviceId:value});
                        });
                    });
                }
            }
        }
        angular.forEach(vm.linkedServices, function(linkedService){
            if(linkedService.servicesIds.length == 0)
                linkedService.servicesIds.push({serviceId:null});
        });
        if(vm.linkedServices.length == 0) vm.linkedServices.push({type: 'addOns',serviceId: null,servicesIds: [{serviceId:null}]});

        function addLinkedService(index){
            vm.linkedServices.splice((index+1),0,{
                type: 'addOns',
                serviceId: null,
                servicesIds: [{serviceId:null}]
            });
        }

        function deleteLinkedService(index){
            vm.linkedServices.splice(index,1);
        }

        function addLinkedOnOptionService(index,array){
            array.splice((index+1),0,{serviceId:null});
        }

        function deleteLinkedOnOptionService(index,array){
            array.splice(index,1);
        }

        function save(){
            vm.dataWasSaved = true;
            vm.service.price = vm.service.price*100;

            vm.service.links = {
                addOns: [],
                during: []
            };
            if(vm.linkedServices.length){
                angular.forEach(vm.linkedServices, function(value){
                    if(value.serviceId && value.serviceId.length){
                        if(value.type == 'addOns') {
                            var onOptions = _.map(value.servicesIds, function(servicesId){ return servicesId.serviceId; }).filter(function(servicesId) { return (servicesId && servicesId.length); });
                            vm.service.links.addOns.push({serviceId:value.serviceId,servicesIds:onOptions});
                        }
                        if(value.type == 'during') vm.service.links.during.push(value.serviceId);
                    }
                });
            }

            vm.saving = true;
            if(vm.addnew) add();
            else update();
        }

        function add(){
            if (checkData()) ServicesService.addService(vm.service).then(function(res) {
                vm.service = res;
                vm.saving = false;
                $state.go('index.services.services');
            }).catch(function(err) {
                console.log(err, 'error');
                vm.errors = 'Can not save your data. '+err.data.message;
                vm.saving = false;
            });
            else {
                vm.errors = 'Please fill all required fields';
                vm.saving = false;
            }
        }

        function update(){
            if (checkData()) ServicesService.updateService(vm.service).then(function(res) {
                vm.service = res;
                vm.saving = false;
                $state.go('index.services.services');
            }).catch(function(err) {
                console.log(err, 'error');
                vm.errors = 'Can not save your data. '+err.data.message;
                vm.saving = false;
            });
            else {
                vm.errors = 'Please fill all required fields';
                vm.saving = false;
            }
        }

        function cancel(){
            vm.saving = false;
            $state.go('index.services.services');
        }

        function checkData(){
            if(
                ( vm.service.name && vm.service.name.length )
                && ( vm.service.duration && vm.service.duration.length && vm.service.duration[0].time>0 )
                && ( vm.service.categoryId && vm.service.categoryId.length )
                && ( vm.service.price>=0 )
                && ( vm.service.order>=0 )
                && ( vm.service.priority>=0 )
            ) return true;
            else return false;
        }

    }
})();
