(function() {
    'use strict';

    angular.module('speedspa.services')
        .controller('ServicesOrdersCtrl', ServicesOrdersCtrl);

    ServicesOrdersCtrl.$inject = [
        'servicesOrdersResolve',
        'staffResolve',
        'ServicesService',
        'QueryDataFilterService',
        '$timeout',
        '$state',
        '$scope',
        '$filter'
    ];

    function ServicesOrdersCtrl(
        servicesOrdersResolve,
        staffResolve,
        ServicesService,
        QueryDataFilterService,
        $timeout,
        $state,
        $scope,
        $filter
    ) {

        var vm = this;

        vm.orders = servicesOrdersResolve;
        vm.filteredOrders = vm.orders;
        vm.staff = staffResolve;
        vm.total = vm.filteredOrders.length;
        vm.statuses = [
            {
                status: 'paid',
                name: 'Paid'
            },
            {
                status: 'created',
                name: 'Created'
            }
        ];
        vm.popupDateAppointment = {
            opened: false,
            dateOptions: {
                formatYear: 'yy',
                startingDay: 1
            }
        };
        vm.searchQuery = {
            dateAppointment: new Date(),
            staff: '',
            status: null
        };
        vm.orderby = {
            field: '',
            desc: true
        };
        vm.buttons = [{
            title: 'Add Order',
            callback: goToAddOrderPage
        }];

        vm.openDateAppointment = openDateAppointment;
        vm.search = search;
        vm.reset = reset;
        vm.order = order;
        vm.delete = ServicesService.deleteOrder;
        vm.summary = summary;

        search();
        order('appTimeStart');

        function openDateAppointment(){
            vm.popupDateAppointment.opened = true;
        }

        function search(){
            var query = {};
            if(vm.searchQuery.staff && vm.searchQuery.staff.length){
                query.staffId = vm.searchQuery.staff;
            }
            if(vm.searchQuery.status && vm.searchQuery.status.length){
                query.status = vm.searchQuery.status;
            }

            vm.filteredOrders = $filter('filter')(vm.orders, query, 'strict');

            if(vm.searchQuery.dateAppointment){
                var start =  new Date(moment(vm.searchQuery.dateAppointment).startOf('day'));
                var end =  new Date(moment(vm.searchQuery.dateAppointment).endOf('day'));
                vm.filteredOrders = $filter('filter')(vm.filteredOrders, moreLessFilter('appTimeStart', 'moreless', start, end));
            }

            vm.total = vm.filteredOrders.length;
            vm.orderby.desc = false;
            order('appTimeStart');
        }

        function reset(){
            vm.searchQuery.staff = '';
            vm.searchQuery.status = null;
            $timeout(function(){
                angular.element('#staffSelect2').select2().val(null).trigger("change");
            }, 100);
            search();
            vm.total = vm.filteredOrders.length;
            vm.orderby.desc = false;
            order('appTimeStart');
        }

        function order(by){
            var oldBy = angular.copy(vm.orderby.field);
            if(by == oldBy){
                vm.orderby.desc = !vm.orderby.desc;
            }
            else {
                vm.orderby.field = by;
                vm.orderby.desc = true;
            }

            vm.filteredOrders = $filter('orderBy')(vm.filteredOrders, vm.orderby.field, vm.orderby.desc);
        }

        function moreLessFilter(prop, moreless, val1, val2){
            return function(item){
                if (moreless == 'more')
                    return item[prop] >= val1;
                else if (moreless == 'less')
                    return item[prop] <= val1;
                else if (moreless == 'moreless'){
                    return (item[prop] >= val1 && item[prop] <= val2);
                }
                else
                    return true;
            }
        }

        function summary(prop){
            var summary = 0;
            var props = prop.split('.');
            function returnProp(value,prop){return value[prop]}

            angular.forEach(vm.filteredOrders, function(value,key){
                var addToSum = value[props[0]];
                if(props.length>1){
                    for(var i=1;i<props.length;i++){
                        addToSum = returnProp(addToSum,props[i]);
                    }
                }
                summary = summary + addToSum;
            });
            return summary;
        }

        function goToAddOrderPage(){
            $state.go('index.services.orders.addnew');
        }

    }
})();
