(function() {
    'use strict';

    angular.module('speedspa.services')
        .controller('ServicesOrderCtrl', ServicesOrderCtrl);

    ServicesOrderCtrl.$inject = [
        'servicesGetOrderResolve',
        'servicesResolve',
        'clientsResolve',
        'ServicesService',
        '$state',
        '$timeout'
    ];

    function ServicesOrderCtrl(
        servicesGetOrderResolve,
        servicesResolve,
        clientsResolve,
        ServicesService,
        $state,
        $timeout
    ) {

        var vm = this;

        vm.order = servicesGetOrderResolve;
        vm.services = servicesResolve;
        vm.clients = clientsResolve;
        vm.title = $state.current.data.bcName;
        vm.errors = '';
        vm.dataWasSaved = false;
        vm.statuses = [
            'canceled',
            'returned',
            'fulfilled'
        ];
        vm.orderItem = {
            service: null,
            quantity: 1
        };
        vm.showTempItem = false;
        vm.addnew = true;
        vm.saving = false;

        vm.save = save;
        vm.cancel = cancel;
        vm.addToOrder = addToOrder;
        vm.removeItem = removeItem;
        vm.findServiceById = findServiceById;
        vm.subTotal = subTotal;

        if($state.params.orderId) {
            vm.addnew = false;
        }
        if( !(_.includes(vm.statuses, vm.order.status)) ){
            vm.statuses.push(vm.order.status);
        }

        if(!vm.addnew && vm.order.appTimeStart){
            vm.order.appTimeStart = new Date(vm.order.appTimeStart);
            vm.order.appTimeStart = new Date(
                vm.order.appTimeStart.getUTCFullYear(),
                vm.order.appTimeStart.getUTCMonth(),
                vm.order.appTimeStart.getUTCDate() ,
                vm.order.appTimeStart.getUTCHours(),
                vm.order.appTimeStart.getUTCMinutes(),
                vm.order.appTimeStart.getUTCSeconds(),
                vm.order.appTimeStart.getUTCMilliseconds()
            );
        }

        function save(){
            vm.dataWasSaved = true;

            vm.saving = true;
            if(vm.addnew) add();
            else update();
        }

        function add(){
            if (checkData())
                ServicesService.addOrder(vm.order).then(function(res) {
                    vm.order = res;
                    vm.saving = false;
                    $state.go('index.services.orders.all');
                }).catch(function(err) {
                    console.log(err, 'error');
                    vm.errors = 'Can not save your data. '+err.data.message;
                    vm.saving = false;
                });
                else {
                    vm.errors = 'Please fill all required fields';
                    vm.saving = false;
                }
        }

        function update(){
            if (checkData()){
                var order = {
                    id: vm.order.id,
                    items: vm.order.items,
                    clientId: vm.order.clientId
                };
                ServicesService.updateOrder(order).then(function(res) {
                    vm.order = res;
                    vm.saving = false;
                    $state.go('index.services.orders.all');
                }).catch(function(err) {
                    console.log(err, 'error');
                    vm.errors = 'Can not save your data. '+err.data.message;
                    vm.saving = false;
                });
            }
            else {
                    vm.errors = 'Please fill all required fields';
                    vm.saving = false;
                }
        }

        function cancel(){
            vm.saving = false;
            $state.go('index.services.orders.all');
        }

        function checkData(){
            if(
                (vm.order.clientId && vm.order.clientId.length >0 )
                && (vm.order.items && vm.order.items.length >0 )
            ) return true;
            else return false;
        }

        function addToOrder(){
            if(vm.orderItem.service){
                var item = {
                    parent: vm.orderItem.service.id,
                    description: vm.orderItem.service.name,
                    quantity: vm.orderItem.quantity
                };
                vm.order.items.push(item);
                vm.orderItem = {
                    service: null,
                    quantity: 1
                };
                $timeout(function(){angular.element('#orderItemParent').select2().val(null).trigger("change");}, 100);
                vm.showTempItem = false;
            }
        }

        function removeItem(index){
            vm.order.items.splice(index,1);
        }

        function findServiceById(id){
            return _.find(vm.services, {id:id});
        }

        function subTotal(){
            var sum = 0;
            angular.forEach(vm.order.items, function(value, key){
                sum = sum + (findServiceById(value.parent)).price;
            });
            return sum;
        }

    }
})();
