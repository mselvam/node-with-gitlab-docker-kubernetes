(function() {
    'use strict';

    angular.module('speedspa.schedule')
        .controller('AddAppointmentCtrl', AddAppointmentCtrl);

    AddAppointmentCtrl.$inject = [
        '$scope',
        'ScheduleService',
        'ServicesService',
        'servicesCategoriesResolve',
        'servicesResolve',
        'staffResolve',
        'clientsResolve',
        '$state',
        '$timeout',
        '$q'
    ];

    function AddAppointmentCtrl(
        $scope,
        ScheduleService,
        ServicesService,
        servicesCategoriesResolve,
        servicesResolve,
        staffResolve,
        clientsResolve,
        $state,
        $timeout,
        $q
    ) {

        var vm = this;

        vm.servicesCategories = servicesCategoriesResolve;
        vm.services = servicesResolve;
        vm.addOnsServices = [];
        vm.processingServices = [];
        vm.onOptionsServices = [];
        vm.staff = staffResolve;
        vm.clients = clientsResolve;
        vm.popupDateTime = {
            opened: false,
            dateOptions: {
                formatYear: 'yy',
                maxDate: new Date(2020, 5, 22),
                minDate: new Date(),
                startingDay: 1
            }
        };
        var emptyEvent = {
            clientId: null,
            serviceMain: null,
            serviceAddOn: null,
            serviceOnOption: null,
            serviceProcessing: null,
            startDate: new Date(),
            startTime: new Date(moment().startOf('day')),
            timeStart: new Date(moment().startOf('day'))
        };
        vm.requiredAddOns = false;
        vm.servicesAreReady = false;
        vm.servicesOptionsAreReady = false;
        vm.dataWasSaved = false;
        vm.event = angular.copy(emptyEvent);
        vm.errors = '';
        vm.success = '';
        vm.saving = false;

        vm.openDate = openDate;
        vm.cancel = cancel;
        vm.save = save;
        vm.categoryStaffList = getCategoryStaffList;
        vm.onChooseMainService = onChooseMainService;
        vm.onChooseAddOnService = onChooseAddOnService;

        function openDate(){
            vm.popupDateTime.opened = true;
        }

        function setDateTimeToSave(date, time){
            var _utc = new Date(date.getFullYear(), date.getMonth(), date.getDate(),  time.getHours(), time.getMinutes(), time.getSeconds());
            _utc.setTime( _utc.getTime() - time.getTimezoneOffset()*60*1000 );
            _utc = _utc.toISOString();
            return _utc;
        }

        function cancel(){
            vm.saving = false;
            $state.go('index.schedule.schedule');
        }

        function save(){
            vm.dataWasSaved = true;
            vm.saving = true;

            var start = setDateTimeToSave(vm.event.startDate, vm.event.startTime);
            var services = [vm.event.serviceMain];
            if(vm.event.serviceAddOn) services.push(vm.event.serviceAddOn);
            if(vm.event.serviceProcessing) services.push(vm.event.serviceProcessing);
            if(vm.event.serviceOnOption) services.push(vm.event.serviceOnOption);

            var servicesObj = _.filter(vm.services, function(serv) { return _.find(services, function(servId){ return serv.id == servId; } ); });
            var servicesForApp = _.values(_.reduce(servicesObj,function(result,obj){
                var name = obj.categoryId;
                var category = _.find(vm.servicesCategories, {'id':obj.categoryId});
                result[name] = {
                    services:(result[name]?result[name].services:[])
                };
                if(vm.event.resourceId && _.find(category.resourcesIds, function(id){return id == vm.event.resourceId;})) {
                    result[name].resourceId = vm.event.resourceId;
                }
                result[name].services.push(obj.id);
                return result;
            },{}));

            var servicesPromise = Promise.resolve("appointment");
            var appointmentsToCreateListF = [];
            vm.errors = ''; vm.success = '';
            angular.forEach(servicesForApp, function(value, key){
                var createAppointment = servicesPromise.then(function () {
                    value.clientId = vm.event.clientId;
                    value.timeStart = start;
                    return ScheduleService.addAppointment(value).then(function(res) {
                        var servicesSaved = _.filter(vm.services, function(serv) { return _.find(res.services, function(servId){ return serv.id == servId; } ); })
                            .reduce(function(services, next) {
                            return services.length?services + ', ' + next.name:services + next.name;
                        }, '');
                        vm.success = vm.success + 'Your appointment for these services was saved: '+servicesSaved+'. Staff name is: '+res.stylistName + ' ';
                        return res;
                    }).catch(function(err) {
                        console.log(err, 'error');
                        vm.errors = vm.errors + 'Can not save your data. '+err.data.message+' ';
                        vm.saving = false;
                        return $q.reject(err);
                    });
                });
                appointmentsToCreateListF.push(createAppointment);
            });

            $q.all(appointmentsToCreateListF).then(function(){
                vm.saving = false;
                $state.go('index.schedule.schedule');
            }).catch(function(err) {
                vm.saving = false;
            });
        }

        function getCategoryStaffList(categoryId){
            if(categoryId){
                var category = _.find(vm.servicesCategories, { 'id': categoryId });
                if(category.resourcesIds) return category.resourcesIds;
                else return [];
            }
            else {
                return false;
            }
        }

        function onChooseMainService(){
            vm.servicesAreReady = false;
            vm.requiredAddOns = false;
            vm.servicesOptionsAreReady = false;
            $timeout(function(){
                vm.addOnsServices = [];
                vm.processingServices = [];
                vm.onOptionsServices = [];
                vm.event.serviceAddOn = null;
                vm.event.serviceOnOption = null;
                vm.event.serviceProcessing = null;
                if(vm.event.serviceMain){
                    var main = _.find(vm.services, { 'id': vm.event.serviceMain });
                    vm.addOnsServices = _.filter(vm.services, function(service){
                        return _.find(main.links.addOns, { 'serviceId': service.id });
                    });
                    vm.processingServices = _.filter(vm.services, function(service){
                        return _.find(main.links.during, function(id) { return id == service.id; });
                    });
                    vm.requiredAddOns = main.requiredAddOns;
                }
                vm.servicesAreReady = true;
            }, 800);
        }

        function onChooseAddOnService(){
            vm.servicesOptionsAreReady = false;
            $timeout(function(){
                vm.onOptionsServices = [];
                vm.event.serviceOnOption = null;
                if(vm.event.serviceAddOn){
                    var main = _.find(vm.services, { 'id': vm.event.serviceMain });
                    var addOn =  _.find(main.links.addOns, { 'serviceId': vm.event.serviceAddOn });
                    vm.onOptionsServices = _.filter(vm.services, function(service){
                        return _.find(addOn.servicesIds, function(id) { return id == service.id; });
                    });
                }
                vm.servicesOptionsAreReady = true;
            }, 800);

        }

    }
})();
