(function() {
    'use strict';
    angular
        .module('speedspa.schedule')
        .factory('ScheduleService', ScheduleService);

    ScheduleService.$inject = [
        'Restangular',
        '$q'
    ];

    function ScheduleService(
        Restangular,
        $q
    ) {
        var service = {
            getSchedule: getSchedule,
            getScheduleItem: getScheduleItem,
            addScheduleItem: addScheduleItem,
            updateScheduleItem: updateScheduleItem,
            deleteScheduleItem: deleteScheduleItem,
            addAppointment: addAppointment,
            getAppointments: getAppointments
        };

        return service;

        function getSchedule(query){
            return Restangular
                .all('timeslots')
                .getList(query)
                .then(getScheduleComplete)
                .catch(getScheduleFailed);

            function getScheduleComplete(res) {
                return res.plain();
            }

            function getScheduleFailed(err) {
                return $q.reject(err);
            }
        }

        function getScheduleItem(id){
            return Restangular
                .all('timeslots')
                .get(id)
                .then(getScheduleItemComplete)
                .catch(getScheduleItemFailed);

            function getScheduleItemComplete(res) {
                return res.plain();
            }

            function getScheduleItemFailed(err) {
                return $q.reject(err);
            }
        }

        function addScheduleItem(item){
            return Restangular
                .all('timeslots')
                .post(item)
                .then(addScheduleItemComplete)
                .catch(addScheduleItemFailed);

            function addScheduleItemComplete(res) {
                return res.plain();
            }

            function addScheduleItemFailed(err) {
                return $q.reject(err);
            }
        }

        function updateScheduleItem(item){
            return Restangular
                .one('timeslots', item.id)
                .patch(item)
                .then(updateScheduleItemComplete)
                .catch(updateScheduleItemFailed);

            function updateScheduleItemComplete(res) {
                return res.plain();
            }

            function updateScheduleItemFailed(err) {
                return $q.reject(err);
            }
        }

        function deleteScheduleItem(item){
            return Restangular
                .one('timeslots', item.id)
                .remove()
                .then(deleteScheduleItemComplete)
                .catch(deleteScheduleItemFailed);

            function deleteScheduleItemComplete(res) {
                return res.plain();
            }

            function deleteScheduleItemFailed(err) {
                return $q.reject(err);
            }
        }

        function addAppointment(item){
            var query = {
                checkSeats: 1,
                checkFreeTime: 1,
                checkResourceSchedule: 1
            };
            return Restangular
                .all('appointments')
                .post(item, query)
                .then(addAppointmentComplete)
                .catch(addAppointmentFailed);

            function addAppointmentComplete(res) {
                return res.plain();
            }

            function addAppointmentFailed(err) {
                return $q.reject(err);
            }
        }

        function getAppointments(query){
            return Restangular
                .all('appointments?'+query)
                .getList({})
                .then(getAppointmentsComplete)
                .catch(getAppointmentsFailed);

            function getAppointmentsComplete(res) {
                return res.plain();
            }

            function getAppointmentsFailed(err) {
                return $q.reject(err);
            }
        }
    }

})();
