(function() {
    'use strict';
    angular
        .module('speedspa.schedule', ['ui.router','ui.calendar', 'ui.bootstrap'])
        .config(routerConfig);

    routerConfig.$inject = [
        '$stateProvider'
    ];

    function routerConfig(
        $stateProvider
    ) {

        $stateProvider
            .state('index.schedule', {
                abstract: true,
                template: "<div data-ui-view></div>",
                data: {
                    proxy: 'index.schedule.schedule',
                    permissions: ['stylist','admin']
                }
            })
            .state('index.schedule.schedule', {
                url: "/schedule",
                templateUrl: "app/schedule/schedule.html",
                controller: 'ScheduleCtrl',
                controllerAs: 'vm',
                data: {
                    bcName: 'Home'
                },
                resolve: {
                    scheduleResolve: scheduleResolve,
                    servicesCategoriesResolve: servicesCategoriesResolve,
                    servicesResolve: servicesResolve,
                    staffResolve: staffResolve
                }
            })
            .state('index.schedule.addappointment', {
                url: "/schedule/add-appointment",
                templateUrl: "app/schedule/add-appointment/add-appointment.html",
                controller: 'AddAppointmentCtrl',
                controllerAs: 'vm',
                data: {
                    bcName: 'Add New Appointment',
                    permissions: ['admin']
                },
                resolve: {
                    servicesCategoriesResolve: servicesCategoriesResolve,
                    servicesResolve: servicesResolve,
                    staffResolve: staffResolve,
                    clientsResolve: clientsResolve
                }
            })
    }

    function scheduleResolve(ScheduleService, $rootScope, $localStorage, QueryDataFilterService, $q){
        var start = new Date(moment().startOf('month').subtract(1, 'milliseconds'));
        start.setTime( start.getTime() - start.getTimezoneOffset()*60*1000 );
        start = start.toISOString();
        var end = new Date(moment().endOf('month').add(1, 'milliseconds'));
        end.setTime( end.getTime() - end.getTimezoneOffset()*60*1000 );
        end = end.toISOString();
        var data = {
            timeStart: {
                $lte: end,
                $gt: start
            }
        };
        if($rootScope.userRole == 'stylist') {
            data.resourceId = {
                '$in': [$localStorage.user.id]
            }
        }
        var query = QueryDataFilterService.buildFilter(data);
        return ScheduleService.getSchedule(query).then(function(res) {
            return res;
        }).catch(function(err) {
            console.log(err, 'error');
            return [];
        });
    }

    function servicesCategoriesResolve(ServicesService, $q){
        return ServicesService.getCategories().then(function(res) {
            return res;
        }).catch(function(err) {
            console.log(err, 'error');
            return [];
        });
    }

    function servicesResolve(ServicesService, $q){
        return ServicesService.getServices().then(function(res) {
            return res;
        }).catch(function(err) {
            console.log(err, 'error');
            return [];
        });
    }

    function staffResolve(StaffService, $rootScope, $localStorage, $q){
        if($rootScope.userRole == 'admin')
            return StaffService.getStaffList('', 'stylist').then(function(res) {
                return res;
            }).catch(function(err) {
                console.log(err, 'error');
                return [];
            });
        else return [$localStorage.user];
    }

    function clientsResolve(ClientsService, $q){
        return ClientsService.getClients().then(function(res) {
            return res;
        }).catch(function(err) {
            console.log(err, 'error');
            return [];
        });
    }

})();
