(function() {
    'use strict';
    angular
        .module('speedspa.messages', [
            'ui.router',
            'ui.mask'
        ])
        .config(routerConfig);

    routerConfig.$inject = [
        '$stateProvider'
    ];

    function routerConfig(
        $stateProvider
    ) {

        $stateProvider
            .state('index.messages', {
                abstract: true,
                url: "/messages",
                template: "<div data-ui-view></div>",
                data: {
                    proxy: 'index.messages.messages',
                    permissions: ['admin']
                }
            })
            .state('index.messages.messages', {
                url: "/",
                templateUrl: "app/messages/messages.html",
                controller: 'MessagesCtrl',
                controllerAs: 'vm',
                data: {
                    bcName: 'Messages'
                },
                resolve: {
                    messageResolve: messageResolve
                }
            })
    }

    function messageResolve(){
        return {
            title: '',
            message: ''
        }
    }

})();
