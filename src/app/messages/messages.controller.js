(function() {
    'use strict';

    angular.module('speedspa.messages')
        .controller('MessagesCtrl', MessagesCtrl);

    MessagesCtrl.$inject = [
        'messageResolve',
        'MessagesService',
        '$localStorage'
    ];

    function MessagesCtrl(
        messageResolve,
        MessagesService,
        $localStorage
    ) {

        var vm = this;

        vm.message = messageResolve;
        vm.success = '';
        vm.errors = '';
        vm.saving = false;

        vm.add = add;
        vm.cancel = cancel;

        function add(){
            if (checkData()){
                vm.message.resourceId = $localStorage.user.id;
                vm.message.createdAt = setDateTimeToSave(new Date());
                console.log(vm.message);
                MessagesService.addMessage(vm.message).then(function(res) {
                    cancel();
                    vm.saving = false;
                    vm.success = 'Thank you! Message was sent!';
                }).catch(function(err) {
                    console.log(err, 'error');
                    vm.errors = 'Can not send your message. '+err.data.message;
                    vm.success = '';
                    vm.saving = false;
                });
            }
            else {
                vm.errors = 'Please fill all required fields';
                vm.success = '';
                vm.saving = false;
            }
        }

        function cancel(){
            vm.message.title = '';
            vm.message.message = '';
        }

        function checkData(){
            if(
                (vm.message.title && vm.message.title.length >0 )
                && ( vm.message.message && vm.message.message.length >0 )
            ) return true;
            else return false;
        }

        function setDateTimeToSave(date){
            date.setTime( date.getTime() - date.getTimezoneOffset()*60*1000 );
            date = date.toISOString();
            return date;
        }

    }
})();
