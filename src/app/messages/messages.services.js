(function() {
    'use strict';
    angular
        .module('speedspa.messages')
        .factory('MessagesService', MessagesService);

    MessagesService.$inject = [
        'Restangular',
        '$q',
        'QueryDataFilterService'
    ];

    function MessagesService(
        Restangular,
        $q,
        QueryDataFilterService
    ) {
        var service = {
            addMessage: addMessage
        };

        return service;

        function addMessage(message){
            return Restangular
                .all('messages')
                .post(message)
                .then(addMessageComplete)
                .catch(addMessageFailed);

            function addMessageComplete(res) {
                return res.plain();
            }

            function addMessageFailed(err) {
                return $q.reject(err);
            }
        }
    }

})();
