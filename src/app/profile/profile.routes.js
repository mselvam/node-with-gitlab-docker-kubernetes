(function() {
    'use strict';
    angular
        .module('speedspa.profile', [
            'ui.router',
            'ui.mask'
        ])
        .config(routerConfig);

    routerConfig.$inject = [
        '$stateProvider'
    ];

    function routerConfig(
        $stateProvider
    ) {

        $stateProvider
            .state('index.profile', {
                abstract: true,
                url: "/profile",
                template: "<div data-ui-view></div>",
                data: {
                    proxy: 'index.profile.profile',
                    permissions: ['admin']
                }
            })
            .state('index.profile.profile', {
                url: "/",
                templateUrl: "app/profile/profile.html",
                controller: 'ProfileCtrl',
                controllerAs: 'vm',
                data: {
                    bcName: 'Profile'
                },
                resolve: {
                    profileResolve: profileResolve
                }
            })
    }

    function profileResolve(StaffService, $q, $localStorage){
        return StaffService.getStaff($localStorage.user.id).then(function(res) {
            return res;
        }).catch(function(err) {
            console.log(err, 'error');
            return {};
        });
    }

})();
