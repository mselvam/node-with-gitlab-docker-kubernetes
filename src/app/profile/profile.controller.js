(function() {
    'use strict';

    angular.module('speedspa.profile')
        .controller('ProfileCtrl', ProfileCtrl);

    ProfileCtrl.$inject = [
        'profileResolve',
        'StaffService',
        '$state',
        'siteConfigs',
        'listOfStates'
    ];

    function ProfileCtrl(
        profileResolve,
        StaffService,
        $state,
        siteConfigs,
        listOfStates
    ) {

        var vm = this;

        if(siteConfigs.envName == 'dev') vm.env = siteConfigs.envName;
        vm.profile = profileResolve;
        vm.USstates = listOfStates;
        vm.gender = [
            {
                value: 'male',
                text: 'Male'
            },
            {
                value: 'female',
                text: 'Female'
            }
        ];
        vm.phoneMask= "(999) 999-9999";
        vm.onlyNumbers = /(^\d{5}$)|(^\d{5}-\d{4}$)/;
        vm.errors = '';
        vm.success = '';
        vm.errorsPass = '';
        vm.successPass = '';
        vm.password = '';
        vm.passwordRep = '';
        vm.dataWasSaved = false;
        vm.saving = false;
        vm.savingPass = false;

        vm.save = save;
        vm.newPass = newPass;
        vm.cancel = cancel;

        if(vm.profile.address){
            if(vm.profile.address.streetAddress){
                vm.profile.address.line1 = vm.profile.address.streetAddress;
                delete vm.profile.address.streetAddress;
            }
            if(vm.profile.address.secondStreetAddress){
                vm.profile.address.line2 = vm.profile.address.secondStreetAddress;
                delete vm.profile.address.secondStreetAddress;
            }
            if(vm.profile.address.zipCode){
                vm.profile.address.postal_code = vm.profile.address.zipCode.toString();
                delete vm.profile.address.zipCode;
            }
        }

        function save(){
            vm.dataWasSaved = true;

            if( !vm.profile.address.secondStreetAddress || vm.profile.address.secondStreetAddress.length == 0 )
                delete vm.profile.address.secondStreetAddress;

            vm.saving = true;
            if (checkData())
                StaffService.updateStaff(vm.profile).then(function(res) {
                    vm.profile = res;
                    vm.saving = false;
                    vm.success = 'Thank you! Your data was saved!';
                }).catch(function(err) {
                    console.log(err, 'error');
                    vm.errors = 'Can not save your data. '+err.data.message;
                    vm.success = '';
                    vm.saving = false;
                });
            else {
                vm.errors = 'Please fill all required fields';
                vm.success = '';
                vm.saving = false;
            }
        }

        function newPass(){
            vm.dataWasSaved = true;

            vm.savingPass = true;
            if (vm.password.length!=0 && vm.passwordRep.length!=0 && (vm.password == vm.passwordRep))
                StaffService.updateStaff({id:vm.profile.id, password:vm.password}).then(function(res) {
                    vm.savingPass = false;
                    vm.password = '';
                    vm.passwordRep = '';
                    vm.successPass = 'Thank you! Your new password was saved!';
                }).catch(function(err) {
                    console.log(err, 'error');
                    vm.errorsPass = 'Can not save your password. '+err.data.message;
                    vm.successPass = '';
                    vm.savingPass = false;
                });
            else {
                vm.errorsPass = 'Please re-enter password, fields are not matching';
                vm.successPass = '';
                vm.savingPass = false;
            }
        }

        function cancel(){
            vm.saving = false;
            $state.go('index.schedule.schedule');
        }

        function checkData(){
            if(
                (vm.profile.firstName && vm.profile.firstName.length >0 )
                && ( vm.profile.lastName && vm.profile.lastName.length >0 )
                && ( vm.profile.phoneNumber && vm.profile.phoneNumber.length >0 )
                && ( vm.profile.email && vm.profile.email.length >0 )
                && vm.profile.address
                && (
                    ( vm.profile.address.line1.length >0 )
                    && ( vm.profile.address.city.length >0 )
                    && ( vm.profile.address.state.length >0 )
                    && ( vm.profile.address.postal_code >0 )
                )
                && ( vm.profile.roles.length >0 )
            ) return true;
            else return false;
        }

    }
})();
