(function() {
    'use strict';

    angular.module('speedspa.staff')
        .controller('StaffCtrl', StaffCtrl);

    StaffCtrl.$inject = [
        'staffResolve',
        'servicesCategoriesResolve',
        'StaffService',
        '$state',
        '$scope',
        '$filter'
    ];

    function StaffCtrl(
        staffResolve,
        servicesCategoriesResolve,
        StaffService,
        $state,
        $scope,
        $filter
    ) {

        var vm = this;

        vm.staff = staffResolve;
        vm.servicesCategories = servicesCategoriesResolve;
        vm.total = vm.staff.length;
        vm.currentPage = 1;
        vm.itemsPerPage = 10;
        vm.buttons = [{
            title: 'Add Staff',
            callback: goToAddStaffPage
        }];
        vm.searchQuery = {
            text: '',
            role: 'all'
        };
        vm.orderby = {
            field: '',
            desc: false
        };
        vm.roles = [
            {
                value: 'all',
                text: 'All'
            },
            {
                value: 'stylist',
                text: 'Stylist'
            },
            {
                value: 'admin',
                text: 'Admin'
            }
        ];

        vm.search = search;
        vm.reset = reset;
        vm.order = order;
        vm.delete = StaffService.deleteStaff;

        normalize();
        order('name');

        function normalize(){
            angular.forEach(vm.staff, function(value, key){
                if(value.profileCategoryId) {
                    var cat = _.find(vm.servicesCategories, {id: value.profileCategoryId});
                    if(cat.id)
                        value.roleName = cat.description? cat.description:cat.name+' Stylist';
                }
                if(!value.roleName)
                    value.roleName = value.roles[0];
            });
        }

        function search(){
            $scope.$emit('loadingStart', 'Loading data was started!');
            StaffService.getStaffList(vm.searchQuery.text, vm.searchQuery.role).then(function(res) {
                vm.staff = res;
                vm.orderby.field = '';
                normalize();
                order('name');
                vm.total = vm.staff.length;
                $scope.$emit('loadingEnd', 'Loading data was ended!');
            }).catch(function(err) {
                console.log(err, 'error');
                vm.staff = [];
                vm.total = 0;
                $scope.$emit('loadingEnd', 'Loading data was ended!');
            })
        }

        function reset(){
            vm.searchQuery = {
                text: '',
                role: 'all'
            };
            search();
        }

        function order(by){
            var oldBy = angular.copy(vm.orderby.field);
            if(by == oldBy){
                vm.orderby.desc = !vm.orderby.desc;
            }
            else {
                vm.orderby.field = by;
                vm.orderby.desc = false;
            }

            vm.staff = $filter('orderBy')(vm.staff, vm.orderby.field, vm.orderby.desc);

            vm.currentPage = 1;
        }

        function goToAddStaffPage(){
            $state.go('index.staff.addnew');
        }

    }
})();
