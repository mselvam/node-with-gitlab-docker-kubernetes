(function() {
    'use strict';
    angular
        .module('speedspa.staff', [
            'ui.router',
            'ui.bootstrap',
            'ui.mask'
        ])
        .config(routerConfig);

    routerConfig.$inject = [
        '$stateProvider'
    ];

    function routerConfig(
        $stateProvider
    ) {

        $stateProvider
            .state('index.staff', {
                abstract: true,
                url: "/staff",
                template: "<div data-ui-view></div>",
                data: {
                    proxy: 'index.staff.staff',
                    permissions: ['admin']
                }
            })
            .state('index.staff.staff', {
                url: "/",
                templateUrl: "app/staff/staff.html",
                controller: 'StaffCtrl',
                controllerAs: 'vm',
                data: {
                    bcName: 'Staff'
                },
                resolve: {
                    staffResolve: staffResolve,
                    servicesCategoriesResolve: servicesCategoriesResolve
                }
            })
            .state('index.staff.addnew', {
                url: "/add-new",
                templateUrl: "app/staff/view-staff/view-staff.html",
                controller: 'ViewStaffCtrl',
                controllerAs: 'vm',
                data: {
                    bcName: 'New Staff'
                },
                resolve: {
                    staffGetStaffResolve: staffGetEmptyStaffResolve,
                    servicesCategoriesResolve: servicesCategoriesResolve
                }
            })
            .state('index.staff.view', {
                url: "/view/:staffId",
                templateUrl: "app/staff/view-staff/view-staff.html",
                controller: 'ViewStaffCtrl',
                controllerAs: 'vm',
                data: {
                    bcName: 'Staff View/Edit'
                },
                resolve: {
                    staffGetStaffResolve: staffGetStaffResolve,
                    servicesCategoriesResolve: servicesCategoriesResolve
                }
            })
    }

    function staffResolve(StaffService, $q){
        return StaffService.getStaffList().then(function(res) {
            return res;
        }).catch(function(err) {
            console.log(err, 'error');
            return [];
        });
    }

    function staffGetEmptyStaffResolve(){
        return {
            firstName: '',
            lastName: '',
            gender: '',
            phoneNumber: '',
            email: '',
            password: '',
            address: {
                line1: '',
                line2: '',
                city: '',
                state: '',
                postal_code: ''
            },
            roles: [],
            profileCategoryId: ''
        }
    }

    function staffGetStaffResolve(StaffService, $q, $stateParams){
        return StaffService.getStaff($stateParams.staffId).then(function(res) {
            return res;
        }).catch(function(err) {
            console.log(err, 'error');
            return {};
        });
    }

    function servicesCategoriesResolve(ServicesService, $q){
        return ServicesService.getCategories().then(function(res) {
            return res;
        }).catch(function(err) {
            console.log(err, 'error');
            return [];
        });
    }

})();
