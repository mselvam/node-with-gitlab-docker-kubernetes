(function() {
    'use strict';

    angular.module('speedspa.staff')
        .controller('ViewStaffCtrl', ViewStaffCtrl);

    ViewStaffCtrl.$inject = [
        'siteConfigs',
        'staffGetStaffResolve',
        'servicesCategoriesResolve',
        'StaffService',
        'ServicesService',
        'ProductsService',
        'listOfStates',
        '$state',
        '$scope',
        '$timeout',
        '$q'
    ];

    function ViewStaffCtrl(
        siteConfigs,
        staffGetStaffResolve,
        servicesCategoriesResolve,
        StaffService,
        ServicesService,
        ProductsService,
        listOfStates,
        $state,
        scope,
        $timeout,
        $q
    ) {

        var vm = this;

        if(siteConfigs.envName == 'dev') vm.env = siteConfigs.envName;
        vm.USstates = listOfStates;
        vm.staff = staffGetStaffResolve;
        vm.servicesCategories = servicesCategoriesResolve;
        vm.title = $state.current.data.bcName;
        vm.role = ( vm.staff.roles && vm.staff.roles.length)? (vm.staff.roles[0]=='admin'? vm.staff.roles[0] : ((vm.staff.profileCategoryId && vm.staff.profileCategoryId.length>0)? vm.staff.profileCategoryId:'')) : '';
        vm.roleAdmin = {
            value: 'admin',
            text: 'Admin'
        };
        vm.gender = [
            {
                value: 'male',
                text: 'Male'
            },
            {
                value: 'female',
                text: 'Female'
            }
        ];
        vm.phoneMask= "(999) 999-9999";
        vm.onlyNumbers = /(^\d{5}$)|(^\d{5}-\d{4}$)/;
        vm.errors = '';
        vm.dataWasSaved = false;
        vm.addnew = true;
        vm.saving = false;
        vm.tempImage = null;
        vm.uploading = false;
        vm.addAvatar = false;

        vm.save = save;
        vm.cancel = cancel;

        if($state.params.staffId) {
            vm.addnew = false;
        }

        if(vm.staff.address){
            if(vm.staff.address.streetAddress){
                vm.staff.address.line1 = vm.staff.address.streetAddress;
                delete vm.staff.address.streetAddress;
            }
            if(vm.staff.address.secondStreetAddress){
                vm.staff.address.line2 = vm.staff.address.secondStreetAddress;
                delete vm.staff.address.secondStreetAddress;
            }
            if(vm.staff.address.zipCode){
                vm.staff.address.postal_code = vm.staff.address.zipCode.toString();
                delete vm.staff.address.zipCode;
            }
        }

        if( !vm.staff.avatar || (vm.staff.avatar && vm.staff.avatar.length == 0) ) vm.addAvatar = true;

        scope.setFile = function(element) {
            scope.$apply(function() {
                vm.tempImage = element.files[0];
            });
            $timeout(function(){
                uploadImage();
            }, 300);
        };

        function uploadImage(){
            var imageToSave = new FormData();
            imageToSave.append("input", vm.tempImage);
            vm.uploading = true;
            ProductsService.uploadImageToProduct(imageToSave).then(function(res) {
                vm.staff.avatar = res.url;
                vm.addAvatar = false;
                vm.uploading = false;
            }).catch(function(err) {
                console.log(err, 'error');
                vm.errors = 'Can not save your data. '+err.data.message;
                vm.uploading = false;
            });
        }

        function save(){
            vm.dataWasSaved = true;

            if(vm.role.length > 0) {
                if(vm.role == 'admin')
                    vm.staff.roles = [vm.role];
                else {
                    vm.staff.roles = ['stylist'];
                    vm.staff.profileCategoryId = vm.role;
                }
            }

            if( !vm.staff.address.secondStreetAddress || vm.staff.address.secondStreetAddress.length == 0 )
                delete vm.staff.address.secondStreetAddress;

            if (!((_.findIndex(vm.staff.roles, function(role){ return (role == 'stylist'); }))+1))
                delete vm.staff.profileCategoryId;

            vm.saving = true;

            if(vm.addnew) add();
            else update();
        }

        function add(){
            if (checkData())
                StaffService.addStaff(vm.staff).then(function(res) {
                    vm.staff = res;

                        // START SAVING STAFF TO SERVICES CATEGORIES
                    if ((_.findIndex(vm.staff.roles, function(role){ return (role == 'stylist'); }))+1) {
                        var categoryToSave = _.find(vm.servicesCategories, function(cat) {
                            return cat.id == vm.staff.profileCategoryId;
                        });
                        if(categoryToSave && categoryToSave.id) {
                            if (categoryToSave.resourcesIds && categoryToSave.resourcesIds.length) {
                                categoryToSave.resourcesIds.push(vm.staff.id);
                            }
                            else categoryToSave.resourcesIds = [vm.staff.id];
                            ServicesService.updateCategory(categoryToSave).then(function(res) {
                                vm.saving = false;
                                $state.go('index.staff.staff');
                            }).catch(function(err) {
                                console.log(err, 'error');
                                vm.errors = 'Can not save category for your staff.';
                                vm.saving = false;
                            });
                        }
                    }
                        // END SAVING STAFF TO SERVICES CATEGORIES
                    else {
                        vm.saving = false;
                        $state.go('index.staff.staff');
                    }


                }).catch(function(err) {
                    console.log(err, 'error');
                    vm.errors = 'Can not save your data. '+err.data.message;
                    vm.saving = false;
                });
                else {
                    vm.errors = 'Please fill all required fields';
                    vm.saving = false;
                }
        }

        function update(){
            if (checkData())
                StaffService.updateStaff(vm.staff).then(function(res) {
                    vm.staff = res;

                        // START SAVING STAFF TO SERVICES CATEGORIES
                    if ((_.findIndex(vm.staff.roles, function(role){ return (role == 'admin'); }))+1) {
                        var categoriesToDeleteList = _.filter(vm.servicesCategories, function(cat) {
                            if(cat.resourcesIds.length)
                                return _.find(cat.resourcesIds, function(resourceId){
                                    return resourceId == vm.staff.id;
                                });
                            else return false;
                        });
                    }
                    if ((_.findIndex(vm.staff.roles, function(role){ return (role == 'stylist'); }))+1) {
                        var categoriesToDeleteList = _.filter(vm.servicesCategories, function (cat) {
                            if (cat.resourcesIds.length)
                                return _.find(cat.resourcesIds, function (resourceId) {
                                    return (resourceId == vm.staff.id && cat.id != vm.staff.profileCategoryId);
                                });
                            else return false;
                        });
                        var categoryToSave = {};
                        angular.forEach(vm.servicesCategories, function (cat, key) {
                            if (cat.id == vm.staff.profileCategoryId) {
                                if (cat.resourcesIds.length) {
                                    var findStaff = (_.findIndex(cat.resourcesIds, function (resourceId) {
                                            return (resourceId == vm.staff.id);
                                        })) + 1;
                                    if (!findStaff)
                                        categoryToSave = cat;
                                }
                                else categoryToSave = cat;
                            }
                        });
                    }

                    var categoryPromise = Promise.resolve("cat");
                    var categoriesToSaveListF = [];

                    if(categoriesToDeleteList && categoriesToDeleteList.length>0) angular.forEach(categoriesToDeleteList, function(value,key){
                        _.remove(value.resourcesIds, function(resourceId) {
                            return resourceId == vm.staff.id;
                        });
                        var deleteFunc = categoryPromise.then(function () {
                            return ServicesService.updateCategory(value).then(function(res) {
                                return res;
                            }).catch(function(err) {
                                console.log(err, 'error');
                                return {};
                            });
                        });
                        categoriesToSaveListF.push(deleteFunc);
                    });
                    if(categoryToSave && categoryToSave.id) {
                        if (categoryToSave.resourcesIds && categoryToSave.resourcesIds.length) {
                            categoryToSave.resourcesIds.push(vm.staff.id);
                        }
                        else categoryToSave.resourcesIds = [vm.staff.id];
                        var saveFunc = categoryPromise.then(function () {
                            return ServicesService.updateCategory(categoryToSave).then(function(res) {
                                return res;
                            }).catch(function(err) {
                                console.log(err, 'error');
                                return {};
                            });
                        });
                        categoriesToSaveListF.push(saveFunc);
                    }
                    $q.all(categoriesToSaveListF).then(function(){
                        vm.saving = false;
                        $state.go('index.staff.staff');
                    },function(err){
                        console.log(err, 'error');
                        vm.errors = 'Can not save category for your staff.';
                        vm.saving = false;
                    });
                        // END SAVING STAFF TO SERVICES CATEGORIES

                }).catch(function(err) {
                    console.log(err, 'error');
                    vm.errors = 'Can not save your data. '+err.data.message;
                    vm.saving = false;
                });
                else {
                    vm.errors = 'Please fill all required fields';
                    vm.saving = false;
                }
        }

        function cancel(){
            vm.saving = false;
            $state.go('index.staff.staff');
        }

        function checkData(){
            if(
                (vm.staff.firstName && vm.staff.firstName.length >0 )
                && ( vm.staff.lastName && vm.staff.lastName.length >0 )
                && ( vm.staff.phoneNumber && vm.staff.phoneNumber.length >0 )
                && ( vm.staff.email && vm.staff.email.length >0 )
                && ( vm.addnew == false || vm.staff.password.length >0 )
                && ( vm.role && vm.role.length >0 )
                && vm.staff.address
                && (
                    ( vm.staff.address.line1.length >0 )
                    && ( vm.staff.address.city.length >0 )
                    && ( vm.staff.address.state.length >0 )
                    && ( vm.staff.address.postal_code.length >0 )
                )
            ) return true;
            else return false;
        }

    }
})();
