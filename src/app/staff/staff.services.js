(function() {
    'use strict';
    angular
        .module('speedspa.staff')
        .factory('StaffService', StaffService);

    StaffService.$inject = [
        'Restangular',
        '$q',
        'QueryDataFilterService'
    ];

    function StaffService(
        Restangular,
        $q,
        QueryDataFilterService
    ) {
        var service = {
            getStaffList: getStaffList,
            getStaff: getStaff,
            addStaff: addStaff,
            updateStaff: updateStaff,
            deleteStaff:deleteStaff
        };

        return service;

        function getStaffList(text, role){
            if(text === undefined){var text = '';}
            if( (role === undefined) || (role == 'all') ){var requestRoles = [
                'admin',
                'stylist'
            ]}
            else {
                var requestRoles = [role];
            }
            var data = {
                'filter': {
                    'roles': {
                        '$in': requestRoles
                    }
                }

            };
            var query = QueryDataFilterService.buildFilter(data);
            query.text = text;
            return Restangular
                .all('users/search')
                .getList(query)
                .then(getStaffListComplete)
                .catch(getStaffListFailed);

            function getStaffListComplete(res) {
                return res.plain();
            }

            function getStaffListFailed(err) {
                return $q.reject(err);
            }
        }

        function getStaff(id){
            return Restangular
                .all('users')
                .get(id)
                .then(getStaffComplete)
                .catch(getStaffFailed);

            function getStaffComplete(res) {
                return res.plain();
            }

            function getStaffFailed(err) {
                return $q.reject(err);
            }
        }

        function addStaff(staff){
            return Restangular
                .all('users')
                .post(staff)
                .then(addStaffComplete)
                .catch(addStaffFailed);

            function addStaffComplete(res) {
                return res.plain();
            }

            function addStaffFailed(err) {
                return $q.reject(err);
            }
        }

        function updateStaff(staff){
            return Restangular
                .one('users', staff.id)
                .patch(staff)
                .then(updateStaffComplete)
                .catch(updateStaffFailed);

            function updateStaffComplete(res) {
                return res.plain();
            }

            function updateStaffFailed(err) {
                return $q.reject(err);
            }
        }

        function deleteStaff(staff){
            return Restangular
                .one('users', staff.id)
                .remove()
                .then(deleteStaffComplete)
                .catch(deleteStaffFailed);

            function deleteStaffComplete(res) {
                return res.plain();
            }

            function deleteStaffFailed(err) {
                return $q.reject(err);
            }
        }
    }

})();
