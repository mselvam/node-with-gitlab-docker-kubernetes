(function() {
  angular
    .module('speedspa')
    .run(run);

  run.$inject = [
      '$http'
  ];

  function run(
      $http
  ) {

    console.info('SpeedSPA is up!');

  }

})();
