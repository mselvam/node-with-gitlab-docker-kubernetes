(function() {
    'use strict';
    angular
        .module('speedspa.dashboard')
        .factory('DashboardService', DashboardService);

    DashboardService.$inject = [
        'Restangular',
        '$q'
    ];

    function DashboardService(
        Restangular,
        $q
    ) {
        var service = {
            getUsers: getUsers
        };

        return service;

        function getUsers(){
            return Restangular
                .all('/auth/users')
                .getList()
                .then(authPhoneLoginComplete)
                .catch(authPhoneLoginFailed);

            function authPhoneLoginComplete(res) {
                return res.plain();
            }

            function authPhoneLoginFailed(err) {
                return $q.reject(err);
            }
        }
    }

})();
