(function() {
    'use strict';
    angular
        .module('speedspa.dashboard', ['ui.router'])
        .config(routerConfig);

    routerConfig.$inject = [
        '$stateProvider'
    ];

    function routerConfig(
        $stateProvider
    ) {

        $stateProvider
            .state('index.dashboard', {
                url: "/dashboard",
                templateUrl: "app/dashboard/dashboard.html",
                controller: 'DashboardCtrl',
                controllerAs: 'vm',
                data: {
                    bcName: 'Home'
                }
            })
    }

})();
