(function() {
    'use strict';

    angular.module('speedspa.clients')
        .controller('ClientCtrl', ClientCtrl);

    ClientCtrl.$inject = [
        'siteConfigs',
        'clientsGetClientResolve',
        'clientsGetClientCardsResolve',
        'ClientsService',
        'listOfStates',
        '$state'
    ];

    function ClientCtrl(
        siteConfigs,
        clientsGetClientResolve,
        clientsGetClientCardsResolve,
        ClientsService,
        listOfStates,
        $state
    ) {

        var vm = this;

        if(siteConfigs.envName == 'dev') vm.env = siteConfigs.envName;
        vm.USstates = listOfStates;
        vm.client = clientsGetClientResolve;
        vm.cards = clientsGetClientCardsResolve;
        vm.title = $state.current.data.bcName;
        vm.role = ( vm.client.roles && vm.client.roles.length)? vm.client.roles[0]:'';
        vm.gender = [
            {
                value: 'male',
                text: 'Male'
            },
            {
                value: 'female',
                text: 'Female'
            }
        ];
        vm.statuses = [
            'active',
            'inactive'
        ];
        vm.phoneMask= "(999) 999-9999";
        vm.onlyNumbers = /(^\d{5}$)|(^\d{5}-\d{4}$)/;
        vm.errors = '';
        vm.dataWasSaved = false;
        vm.saving = false;
        vm.activeTab = 0;

        vm.save = save;
        vm.cancel = cancel;
        vm.checkData = checkData;
        vm.setActiveTab = setActiveTab;
        vm.addnew = true;

        if($state.params.clientId) {
            vm.addnew = false;
        }

        if(!vm.client.status){
            vm.client.status = 'active';
        }

        if(vm.client.address){
            if(vm.client.address.streetAddress){
                vm.client.address.line1 = vm.client.address.streetAddress;
                delete vm.client.address.streetAddress;
            }
            if(vm.client.address.secondStreetAddress){
                vm.client.address.line2 = vm.client.address.secondStreetAddress;
                delete vm.client.address.secondStreetAddress;
            }
            if(vm.client.address.zipCode){
                vm.client.address.postal_code = vm.client.address.zipCode.toString();
                delete vm.client.address.zipCode;
            }
        }

        function save(){
            vm.dataWasSaved = true;

            if( vm.client.address ){
                if (
                    (!vm.client.address.line1 || vm.client.address.line1.length == 0)
                    && (!vm.client.address.line2 || vm.client.address.line2.length == 0)
                    && (!vm.client.address.city || vm.client.address.city.length == 0)
                    && (!vm.client.address.state || vm.client.address.state.length == 0)
                    && (!vm.client.address.postal_code || vm.client.address.postal_code.length == 0)
                )
                    delete vm.client.address;
                else {
                    if (!vm.client.address.line1 || vm.client.address.line1.length == 0)
                        delete vm.client.address.line1;
                    if (!vm.client.address.line2 || vm.client.address.line2.length == 0)
                        delete vm.client.address.line2;
                    if (!vm.client.address.city || vm.client.address.city.length == 0)
                        delete vm.client.address.city;
                    if (!vm.client.address.state || vm.client.address.state.length == 0)
                        delete vm.client.address.state;
                    if (!vm.client.address.postal_code || vm.client.address.postal_code.length == 0)
                        delete vm.client.address.postal_code;
                }
            }

            if (!vm.client.email || vm.client.email.length == 0)
                delete vm.client.email;
            if (!vm.client.password || vm.client.password.length == 0)
                delete vm.client.password;
            if (!vm.client.status || vm.client.status.length == 0)
                delete vm.client.status;
            if (!vm.client.gender || vm.client.gender.length == 0)
                delete vm.client.gender;

            vm.saving = true;
            if(vm.addnew) add();
            else update();
        }

        function add(){
            if (checkData())
                ClientsService.addClient(vm.client).then(function(res) {
                    vm.client = res;
                    vm.saving = false;
                    $state.go('index.clients.clients');
                }).catch(function(err) {
                    console.log(err, 'error');
                    vm.errors = 'Can not save your data. '+err.data.message;
                    vm.saving = false;
                });
                else {
                    vm.errors = 'Please fill all required fields';
                    vm.saving = false;
                }
        }

        function update(){
            if (checkData())
                ClientsService.updateClient(vm.client).then(function(res) {
                    vm.client = res;
                    vm.saving = false;
                    $state.go('index.clients.clients');
                }).catch(function(err) {
                    console.log(err, 'error');
                    vm.errors = 'Can not save your data. '+err.data.message;
                    vm.saving = false;
                });
                else {
                    vm.errors = 'Please fill all required fields';
                    vm.saving = false;
                }
        }

        function cancel(){
            vm.saving = false;
            $state.go('index.clients.clients');
        }

        function checkData(){
            if(
                (vm.client.firstName && vm.client.firstName.length >0 )
                && ( vm.client.lastName && vm.client.lastName.length >0 )
                && ( vm.client.phoneNumber && vm.client.phoneNumber.length >0 )
            ) return true;
            else return false;
        }

        function setActiveTab(e, index){
            vm.activeTab = index;
        }

    }
})();
