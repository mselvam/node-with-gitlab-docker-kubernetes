(function() {
    'use strict';

    angular.module('speedspa.core')
        .directive('clientCards', clientCards);

    clientCards.$inject = [];

    function clientCards() {
        var directive = {
            restrict: 'E',
            replace: true,
            templateUrl: 'app/clients/client/client-cards/client-cards.html',
            scope: true,
            controller: clientCardsCtrl,
            controllerAs: 'vm',
            bindToController: {
                clientId: '=',
                cards: '='
            }
        };

        return directive;
    }

    clientCardsCtrl.inject = [
        'ClientsService',
        '$timeout',
        '$anchorScroll',
        'siteConfigs'
    ];

    function clientCardsCtrl(
        ClientsService,
        $timeout,
        $anchorScroll,
        siteConfigs
    ){

        var vm = this;

        vm.add = add;
        vm.save = save;
        vm.edit = edit;
        vm.cancel = cancel;
        vm.openDate = openDate;
        vm.delete = ClientsService.removeClientCard;

        vm.cardOwner = '';
        vm.card = {};
        vm.addnew = true;
        vm.errors = '';
        vm.popupDateTime = {
            opened: false,
            dateOptions: {
                minMode: 'month',
                formatYear: 'yyyy',
                startingDay: 1
            }
        };
        var stripe = Stripe(siteConfigs.strToken);
        var elements = stripe.elements();
        var style = {
            base: {
                color: '#303238',
                fontSize: '16px',
                lineHeight: '48px',
                fontSmoothing: 'antialiased',
                '::placeholder': {
                    color: '#ccc'
                }
            },
            invalid: {
                color: '#e5424d',
                ':focus': {
                    color: '#303238'
                }
            }
        };
        vm.cardNew = elements.create('card',{style:style});
        vm.cardNew.mount('#card-element');
        vm.saving = false;

        function add(){

            vm.saving = true;
            stripe.createToken(vm.cardNew, {
                name: vm.cardOwner
            }).then(function(result) {
                // handle result.error or result.source

                if (result.error) { // Problem!

                    vm.errors = 'Can not save your data. Please double check all fields.';
                    vm.saving = false;

                } else { // Source was created!

                    vm.errors = '';

                    // Get the source ID:
                    var token = result.token.id;

                    ClientsService.createClientCard(vm.clientId,{source:token}).then(function(res) {

                        vm.cardNew.unmount('#card-element');
                        vm.cardNew.mount('#card-element');
                        vm.cardOwner = '';
                        vm.saving = false;

                        ClientsService.getClientCards(vm.clientId).then(function(res) {
                            vm.cards = res.data? res.data:[];
                        }).catch(function(err) {
                            console.log(err, 'error');
                            return [];
                        });
                    }).catch(function(err) {
                        vm.errors = 'Can not save your data. '+err.data.message;
                        console.log(err, 'error');
                        vm.saving = false;
                    });

                }
            });

        }

        function save(){

            var cardToSave = {
                name: vm.card.name,
                exp_month: (moment(vm.card.date).month()+1),
                exp_year: moment(vm.card.date).year()
            };

            vm.saving = true;
            ClientsService.updateClientCard(vm.clientId,vm.card.id,cardToSave).then(function(res) {
                var cardIndex;
                _.map(vm.cards, function(card, index){
                    if( card.id == res.id ) {
                        cardIndex = index;
                    }
                });
                if(cardIndex!=='undefined'){
                    vm.cards[cardIndex].name = res.name;
                    vm.cards[cardIndex].exp_month = res.exp_month;
                    vm.cards[cardIndex].exp_year = res.exp_year;
                }
                vm.saving = false;
                cancel();
            }).catch(function(err) {
                vm.errors = 'Can not save your data. '+err.data.message;
                console.log(err, 'error');
                vm.saving = false;
            });

        }

        function edit(card){
            vm.card = card;
            vm.card.date = moment().month((vm.card.exp_month-1)).year(vm.card.exp_year).date(15);
            vm.card.date = new Date(vm.card.date);
            vm.addnew = false;
            vm.cardNew.unmount('#card-element');
            vm.cardOwner = '';

            $timeout(function(){$anchorScroll('edit-card');}, 300);
        }

        function cancel(){
            vm.errors = '';
            vm.card = {};
            vm.addnew = true;
            vm.cardNew.mount('#card-element');
        }

        function openDate(){
            vm.popupDateTime.opened = true;
        }

    }
})();
