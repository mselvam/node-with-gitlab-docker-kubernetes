(function() {
    'use strict';
    angular
        .module('speedspa.clients', [
            'ui.router',
            'ui.mask'
        ])
        .config(routerConfig);

    routerConfig.$inject = [
        '$stateProvider'
    ];

    function routerConfig(
        $stateProvider
    ) {

        $stateProvider
            .state('index.clients', {
                abstract: true,
                url: "/clients",
                template: "<div data-ui-view></div>",
                data: {
                    proxy: 'index.clients.clients',
                    permissions: ['admin']
                }
            })
            .state('index.clients.clients', {
                url: "/",
                templateUrl: "app/clients/clients.html",
                controller: 'ClientsCtrl',
                controllerAs: 'vm',
                data: {
                    bcName: 'Clients'
                },
                resolve: {
                    clientsResolve: clientsResolve
                }
            })
            .state('index.clients.addnew', {
                url: "/add-new",
                templateUrl: "app/clients/client/client.html",
                controller: 'ClientCtrl',
                controllerAs: 'vm',
                data: {
                    bcName: 'New Client'
                },
                resolve: {
                    clientsGetClientResolve: clientsGetEmptyClientResolve,
                    clientsGetClientCardsResolve: clientsGetEmptyClientCardsResolve
                }
            })
            .state('index.clients.view', {
                url: "/view/:clientId",
                templateUrl: "app/clients/client/client.html",
                controller: 'ClientCtrl',
                controllerAs: 'vm',
                data: {
                    bcName: 'Client View/Edit'
                },
                resolve: {
                    clientsGetClientResolve: clientsGetClientResolve,
                    clientsGetClientCardsResolve: clientsGetClientCardsResolve
                }
            })
    }

    function clientsResolve(ClientsService, $q){
        return ClientsService.getClients().then(function(res) {
            return res;
        }).catch(function(err) {
            console.log(err, 'error');
            return [];
        });
    }

    function clientsGetEmptyClientResolve(){
        return {
            firstName: '',
            lastName: '',
            gender: '',
            phoneNumber: '',
            email: '',
            password: '',
            status: 'active',
            address: {
                line1: '',
                line2: '',
                city: '',
                state: '',
                postal_code: ''
            },
            roles: [
                'client'
            ],
            notes: ''
        }
    }

    function clientsGetEmptyClientCardsResolve(){
        return []
    }

    function clientsGetClientResolve(ClientsService, $q, $stateParams){
        return ClientsService.getClient($stateParams.clientId).then(function(res) {
            return res;
        }).catch(function(err) {
            console.log(err, 'error');
            return {};
        });
    }

    function clientsGetClientCardsResolve(ClientsService, $q, $stateParams){
        return ClientsService.getClientCards($stateParams.clientId).then(function(res) {
            return res.data? res.data:[];
        }).catch(function(err) {
            console.log(err, 'error');
            return [];
        });
    }

})();
