(function() {
    'use strict';

    angular.module('speedspa.clients')
        .controller('ClientsCtrl', ClientsCtrl);

    ClientsCtrl.$inject = [
        'clientsResolve',
        'ClientsService',
        '$state',
        '$scope',
        '$filter'
    ];

    function ClientsCtrl(
        clientsResolve,
        ClientsService,
        $state,
        $scope,
        $filter
    ) {

        var vm = this;

        vm.clients = clientsResolve;
        vm.total = vm.clients.length;
        vm.currentPage = 1;
        vm.itemsPerPage = 10;
        vm.searchQuery = {
            text: '',
            status: ''
        };
        vm.orderby = {
            field: '',
            desc: false
        };
        vm.statuses = [
            {
                value: '',
                text: 'All'
            },
            {
                value: 'active',
                text: 'Active'
            },
            {
                value: 'inactive',
                text: 'Inactive'
            }
        ];
        vm.buttons = [{
            title: 'Add Client',
            callback: goToAddClientPage
        }];

        vm.search = search;
        vm.reset = reset;
        vm.order = order;
        vm.delete = ClientsService.deleteClient;

        order('name');

        function search(){
            $scope.$emit('loadingStart', 'Loading data was started!');
            ClientsService.getClients(vm.searchQuery.text, vm.searchQuery.status).then(function(res) {
                vm.clients = res;
                vm.total = vm.clients.length;
                vm.orderby.field = '';
                order('name');
                $scope.$emit('loadingEnd', 'Loading data was ended!');
            }).catch(function(err) {
                console.log(err, 'error');
                vm.clients = [];
                vm.total = 0;
                $scope.$emit('loadingEnd', 'Loading data was ended!');
            })
        }

        function reset(){
            vm.searchQuery = {
                text: '',
                status: ''
            };
            search();
        }

        function order(by){
            var oldBy = angular.copy(vm.orderby.field);
            if(by == oldBy){
                vm.orderby.desc = !vm.orderby.desc;
            }
            else {
                vm.orderby.field = by;
                vm.orderby.desc = false;
            }

            vm.clients = $filter('orderBy')(vm.clients, vm.orderby.field, vm.orderby.desc);

            vm.currentPage = 1;
        }

        function goToAddClientPage(){
            $state.go('index.clients.addnew');
        }

    }
})();
