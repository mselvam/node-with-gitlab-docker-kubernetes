(function() {
    'use strict';
    angular
        .module('speedspa.clients')
        .factory('ClientsService', ClientsService);

    ClientsService.$inject = [
        'Restangular',
        '$q',
        'QueryDataFilterService'
    ];

    function ClientsService(
        Restangular,
        $q,
        QueryDataFilterService
    ) {
        var service = {
            getClients: getClients,
            getClient: getClient,
            addClient: addClient,
            updateClient: updateClient,
            deleteClient: deleteClient,
            getClientCards: getClientCards,
            getClientCard: getClientCard,
            createClientCard: createClientCard,
            updateClientCard: updateClientCard,
            removeClientCard: removeClientCard
        };

        return service;

        function getClients(text, status){
            if(text === undefined){var text = '';}
            var data = {
                'filter': {
                    'roles': 'client'
                }

            };
            if(status && status.length){
                data.filter.status = {
                    $in: [status]
                }
            }
            var query = QueryDataFilterService.buildFilter(data);
            query.text = text;
            return Restangular
                .all('users/search')
                .getList(query)
                .then(getClientsComplete)
                .catch(getClientsFailed);

            function getClientsComplete(res) {
                return res.plain();
            }

            function getClientsFailed(err) {
                return $q.reject(err);
            }
        }

        function getClient(id){
            return Restangular
                .all('users')
                .get(id)
                .then(getClientComplete)
                .catch(getClientFailed);

            function getClientComplete(res) {
                return res.plain();
            }

            function getClientFailed(err) {
                return $q.reject(err);
            }
        }

        function addClient(client){
            return Restangular
                .all('users')
                .post(client)
                .then(addClientComplete)
                .catch(addClientFailed);

            function addClientComplete(res) {
                return res.plain();
            }

            function addClientFailed(err) {
                return $q.reject(err);
            }
        }

        function updateClient(client){
            return Restangular
                .one('users', client.id)
                .patch(client)
                .then(updateClientComplete)
                .catch(updateClientFailed);

            function updateClientComplete(res) {
                return res.plain();
            }

            function updateClientFailed(err) {
                return $q.reject(err);
            }
        }

        function deleteClient(client){
            return Restangular
                .one('users', client.id)
                .remove()
                .then(deleteClientComplete)
                .catch(deleteClientFailed);

            function deleteClientComplete(res) {
                return res.plain();
            }

            function deleteClientFailed(err) {
                return $q.reject(err);
            }
        }

        function getClientCards(userId){
            return Restangular
                .one('users/'+userId+'/cards')
                .get()
                .then(getClientCardsComplete)
                .catch(getClientCardsFailed);

            function getClientCardsComplete(res) {
                return res.plain();
            }

            function getClientCardsFailed(err) {
                return $q.reject(err);
            }
        }

        function getClientCard(userId, cardId){
            return Restangular
                .one('users/'+userId+'/cards/'+cardId)
                .get()
                .then(getClientCardComplete)
                .catch(getClientCardFailed);

            function getClientCardComplete(res) {
                return res.plain();
            }

            function getClientCardFailed(err) {
                return $q.reject(err);
            }
        }

        function createClientCard(userId,card){
            return Restangular
                .one('users/'+userId+'/cards')
                .post('',card)
                .then(createClientCardComplete)
                .catch(createClientCardFailed);

            function createClientCardComplete(res) {
                return res.plain();
            }

            function createClientCardFailed(err) {
                return $q.reject(err);
            }
        }

        function updateClientCard(userId,cardId,card){
            return Restangular
                .one('users/'+userId+'/cards/'+cardId)
                .patch(card)
                .then(updateClientCardComplete)
                .catch(updateClientCardFailed);

            function updateClientCardComplete(res) {
                return res.plain();
            }

            function updateClientCardFailed(err) {
                return $q.reject(err);
            }
        }

        function removeClientCard(card){
            return Restangular
                .one('users/'+card.userId+'/cards/'+card.id)
                .remove()
                .then(removeClientCardComplete)
                .catch(removeClientCardFailed);

            function removeClientCardComplete(res) {
                return res.plain();
            }

            function removeClientCardFailed(err) {
                return $q.reject(err);
            }
        }
    }

})();
