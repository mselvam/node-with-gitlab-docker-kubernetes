(function (window) {
    window.__env = window.__env || {};

    // API url
    window.__env.api = 'http://server.dev.speedspa.tep.pw';

    // Api url for uploading images
    window.__env.uploadApi = 'http://upload.dev.speedspa.tep.pw';

    // Stripe token
    window.__env.strToken = 'pk_test_ZxH38kklVu6asEYKtsSPqk0f';

    // Environment name
    window.__env.envName = 'staging';

    // Whether or not to enable debug mode
    // Setting this to false will disable console output
    window.__env.enableDebug = true;
}(this));
