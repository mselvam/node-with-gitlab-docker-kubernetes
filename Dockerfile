FROM node:8.1.2-alpine

RUN apk add --no-cache git

# Create app directory
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app
COPY . /usr/src/app

RUN npm install && \
    npm run bower-install && \
    npm run gulp build-prod && \
    rm -rf ./src && \
    npm prune --production

EXPOSE 8080
CMD [ "npm", "start" ]
